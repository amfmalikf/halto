-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 02, 2018 at 06:55 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fc_hama_penyakit`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_gejala`
--

CREATE TABLE IF NOT EXISTS `detail_gejala` (
  `id_HamaPenyakit` int(11) NOT NULL,
  `id_gejala` int(11) NOT NULL,
  `gejala` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `img_gejala` text COLLATE latin1_general_ci NOT NULL,
  `statusG` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `detail_gejala`
--

INSERT INTO `detail_gejala` (`id_HamaPenyakit`, `id_gejala`, `gejala`, `img_gejala`, `statusG`) VALUES
(1806250001, 1, 'Serangan pada buah muda menyebabkan buah tidak berkembang, lama kelamaan berwarna kuning kemerahan kemudian gugur.', 'gejala 1 PBko.PNG', 0),
(1806250001, 2, 'Serangan pada buah tua menyebabkan biji kopi rusak. jika dibelah, terdapat larva, pupa dan imago PBKo.', 'gejala 2 PBko.PNG', 0),
(1806250001, 3, 'Serangan pada buah tua, mengakibatkan biji berlubang sehingga menurunkan mutu kopi.', 'gejala 3 PBko.PNG', 0),
(1806250002, 4, '(a) buah dan (b) bunga kopi yang terserang kutu putih mengering dan gugur. serangan pada buah tua mengakibatkan buah mengerut dan masak sebelum waktunya', 'G kutu putih.PNG', 0),
(1806250003, 5, 'daun kopi terlihat menguning dan  adanya liang gerekan pada batang disertai dengan adanya kotoran yang berwarna merah kehitam-hitaman yang keluar dari liang gerekan( lubang aktif)', 'G Penggerek batang.PNG', 0),
(1806250004, 6, 'bercak berwarna kuning muda pada permukaan bawah daun kemudian  berubah menjadi kuning tua dan terbentuk tepung', 'Karat daun kopi.PNG', 0),
(1806250004, 7, 'daun yang terserang gugur sebelum waktunya. serangan berat dapat menyebabkan pohon menjadi gundul, cabang/ranting mati dan akhirnya tanaman mati. penyakit ini menyerang kopi arabika karena memiliki ka', 'G Karat daun kopi.PNG', 0),
(1806250005, 8, 'terdapat bercak berwarna hitam yang meluas hingga seluruh buah berwarna hitam', 'G Busuk Buah.PNG', 0),
(1806250005, 9, 'terdapat spora berwarna putih yang merupakan spora jamur. Jamur yang menyerang dapat berasal lebih dari 1 genus, yaitu Fusarium, Antraknosa, maupun Phytophthora', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `filegambar_video`
--

CREATE TABLE IF NOT EXISTS `filegambar_video` (
  `id_HamaPenyakit` int(11) NOT NULL,
  `gambar_video` text COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `filegambar_video`
--

INSERT INTO `filegambar_video` (`id_HamaPenyakit`, `gambar_video`) VALUES
(1806250001, 'penggerek Buah Kopi-PBKo.PNG'),
(1806250002, 'kutu putih.PNG'),
(1806250003, 'Penggerek batang(Zeuzera Coffeae).PNG'),
(1806250004, 'Karat daun kopi.PNG'),
(1806250005, 'Busuk Buah.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `hama_penyakit`
--

CREATE TABLE IF NOT EXISTS `hama_penyakit` (
  `id_user` int(11) NOT NULL,
  `id_HamaPenyakit` int(11) NOT NULL,
  `nama_HamaPenyakit` varchar(40) COLLATE latin1_general_ci NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `hama_penyakit`
--

INSERT INTO `hama_penyakit` (`id_user`, `id_HamaPenyakit`, `nama_HamaPenyakit`, `tanggal`) VALUES
(1, 1806250001, 'Penggerek buah Kopi/PBKo', '2018-06-25'),
(1, 1806250002, 'Kutu Putih', '2018-06-25'),
(1, 1806250003, 'Penggerek batang(Zeuzera Coffeae)', '2018-06-25'),
(1, 1806250004, 'Karat daun kopi', '2018-06-25'),
(1, 1806250005, 'Busuk Buah', '2018-06-25');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `ikon` varchar(50) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`, `ikon`) VALUES
(1, 'Kopi', '');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi_serangan`
--

CREATE TABLE IF NOT EXISTS `lokasi_serangan` (
  `id` int(11) NOT NULL,
  `kategori` int(11) NOT NULL,
  `nama_opt` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `alamat` text COLLATE latin1_general_ci NOT NULL,
  `tahun` date NOT NULL,
  `latittude` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `longitude` varchar(50) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `lokasi_serangan`
--

INSERT INTO `lokasi_serangan` (`id`, `kategori`, `nama_opt`, `alamat`, `tahun`, `latittude`, `longitude`) VALUES
(1, 1, 'sjhfjah', 'shfjshj', '0000-00-00', '-4.840318', '105.481820');

-- --------------------------------------------------------

--
-- Table structure for table `pengendalian`
--

CREATE TABLE IF NOT EXISTS `pengendalian` (
  `id_HamaPenyakit` int(11) NOT NULL,
  `id_pengendalian` int(11) NOT NULL,
  `teknik` int(11) NOT NULL,
  `nama` text COLLATE latin1_general_ci NOT NULL,
  `gambar` text COLLATE latin1_general_ci NOT NULL,
  `deskripsi` text COLLATE latin1_general_ci NOT NULL,
  `keterangan` text COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `pengendalian`
--

INSERT INTO `pengendalian` (`id_HamaPenyakit`, `id_pengendalian`, `teknik`, `nama`, `gambar`, `deskripsi`, `keterangan`) VALUES
(1806210005, 1, 1, 'efe', 'abstract-colorful-vector-background-artwork.jpg', 'fef', 'fef'),
(1806210006, 2, 1, 'dwd', 'abstract-colorful-vector-background-artwork.jpg', 'dwd', 'dwdw'),
(1806210007, 3, 1, 'dwd', 'abstract paper textures artwork backgrounds cracks 1920x1200 wallpaper_wallpaperswa.com_98.jpg', 'dwdw', 'wdw'),
(1806210008, 4, 1, 'dede', 'abstract-colorful-vector-background-artwork.jpg', 'fefefe', 'sfefe'),
(1806220001, 5, 1, 'qeqe', 'abstract-colorful-vector-background-artwork.jpg', 'qeqe', 'qeqe'),
(1806220002, 6, 2, 'jbhjbjh', 'abstract-colorful-vector-background-artwork.jpg', 'kkjkj', 'fgfgc'),
(1806220003, 7, 1, 'nkcjnsjncjs', '046dd166da12863ce4e5add451e8f7cc.jpg', 'wefwef', 'efew'),
(1806220003, 8, 1, 'fefe', 'abstract paper textures artwork backgrounds cracks 1920x1200 wallpaper_wallpaperswa.com_98.jpg', 'efwe', 'wefe'),
(1806250001, 9, 1, 'Petik buah', 'p1 PBKo.PNG', 'Semua buah kopi yang berlubang dipetik secara rutin sebulan sekali', 'Memutuskan Siklus hama dengan cara mengambil semua buah kopi baik yang masih terdapat pada pohon maupun yang jatuh ke tanah'),
(1806250001, 10, 1, 'Rampasan', 'p2 PBKo.PNG', 'Memetik semua buah kopi yang masih berada dipohon pada akhir panen', 'Memutuskan Siklus hama dengan cara mengambil semua buah kopi baik yang masih terdapat pada pohon maupun yang jatuh ke tanah'),
(1806250001, 11, 1, 'Lelesan', 'p3 PBKo.PNG', 'Buah kopi yang jatuh di tanah, baik buah terserang maupun tidak terserang PBKo, dikumpulkan', 'Memutuskan Siklus hama dengan cara mengambil semua buah kopi baik yang masih terdapat pada pohon maupun yang jatuh ke tanah'),
(1806250001, 12, 2, 'pemanfaatan musuh alami', 'p4 PBKo.PNG', 'a. imago parasitoid cephalonomia stephanoderis(Hym:Bethylidae) memparasitisasi dengan cara meletakkan telur pada larva PBKo. imago berwarna hitam berukuran 1.5 - 2', ''),
(1806250002, 13, 1, 'Memonitoring secara instensif untuk mengetahui ambang kendali', 'G kutu putih.PNG', '', ''),
(1806250002, 14, 1, 'pengaturan  naungan serta memangkas cabang tanaman kopi yang menyantuh tanah untuk mencegah keberadaan semut', 'G kutu putih.PNG', '', ''),
(1806250003, 15, 1, 'Cara Pengendalian', 'P Penggerek batang.PNG', '', ''),
(1806250004, 16, 1, 'cara pengendalian', 'P Karat daun kopi.PNG', '', ''),
(1806250005, 17, 1, 'Cara Pengendalian', 'P Busuk Buah.PNG', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `alamat` text COLLATE latin1_general_ci NOT NULL,
  `telp` char(13) COLLATE latin1_general_ci NOT NULL,
  `jenis_kelamin` int(11) NOT NULL,
  `username` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_gejala`
--
ALTER TABLE `detail_gejala`
  ADD KEY `id_gejala` (`id_gejala`);

--
-- Indexes for table `hama_penyakit`
--
ALTER TABLE `hama_penyakit`
  ADD PRIMARY KEY (`id_HamaPenyakit`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `lokasi_serangan`
--
ALTER TABLE `lokasi_serangan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengendalian`
--
ALTER TABLE `pengendalian`
  ADD KEY `id_pengendalian` (`id_pengendalian`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_gejala`
--
ALTER TABLE `detail_gejala`
  MODIFY `id_gejala` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lokasi_serangan`
--
ALTER TABLE `lokasi_serangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pengendalian`
--
ALTER TABLE `pengendalian`
  MODIFY `id_pengendalian` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
