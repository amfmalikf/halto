<?php 

class Login extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('m_login');

	}

	function index(){
		$this->load->view('auth');
	}

	function aksi_login(){
		$username = $this->input->post('nama');
		$password = $this->input->post('password');
		$where = array(
			'nama' => $username,
			'password' => $password
			);
		$cek = $this->m_login->cek_login("Tbl_user",$where)->num_rows();
		if($cek > 0){

			$data_session = array(
				'name' => $username,
				'stat' => "login",
				'namalengkap'=> $this->m_login->profil($username)->name,
				'akses'=> $this->m_login->profil($username)->posisi,
				);

			$this->session->set_userdata($data_session);

			redirect(base_url("admin"));

		}else{
			echo "<script>alert('Username atau Password Salah')
			window.location = '".base_url('login')."'</script>";

		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}
}