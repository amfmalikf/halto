
<h2>Tbl Noreg Sapi</h2>

<?php 
    echo $this->session->flashdata('notify');
?>


<!-- <div class="row">
    <div class="col-lg-12 col-md-12">        
        <?php 
                
                echo create_breadcrumb();        
                echo $this->session->flashdata('notify');
                
    ?>
    </div>
</div> -->

<?php echo form_open(site_url('tbl_noreg_sapi/' . $action),'role="form" class="form-horizontal" id="form_tbl_noreg_sapi" parsley-validate'); ?>               
<div class="panel panel-default">
    <div class="panel-heading"><i class="glyphicon glyphicon-edit"></i> </div>
     
      <div class="panel-body">
         
                       
               <div class="form-group">
                   <label for="kelas" class="col-sm-2 control-label">Kelas</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'kelas',
                                 'id'           => 'kelas',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Kelas',
                                 'maxlength'=>'1'
                                 ),
                                 set_value('kelas',$tbl_noreg_sapi['kelas'])
                           );             
                  ?>
                 <?php echo form_error('kelas');?>
                </div>
              </div> <!--/ Kelas -->
                          
               <div class="form-group">
                   <label for="harga_dasar" class="col-sm-2 control-label">Harga Dasar</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'harga_dasar',
                                 'id'           => 'harga_dasar',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Harga Dasar',
                                 'maxlength'=>'11'
                                 ),
                                 set_value('harga_dasar',$tbl_noreg_sapi['harga_dasar'])
                           );             
                  ?>
                 <?php echo form_error('harga_dasar');?>
                </div>
              </div> <!--/ Harga Dasar -->
                          
               <div class="form-group">
                   <label for="gambar" class="col-sm-2 control-label">Gambar</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'gambar',
                                 'id'           => 'gambar',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Gambar',
                                 
                                 ),
                                 set_value('gambar',$tbl_noreg_sapi['gambar'])
                           );             
                  ?>
                 <?php echo form_error('gambar');?>
                </div>
              </div> <!--/ Gambar -->
                          
               <div class="form-group">
                   <label for="status" class="col-sm-2 control-label">Status</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'status',
                                 'id'           => 'status',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Status',
                                 'maxlength'=>'2'
                                 ),
                                 set_value('status',$tbl_noreg_sapi['status'])
                           );             
                  ?>
                 <?php echo form_error('status');?>
                </div>
              </div> <!--/ Status -->
                          
               <div class="form-group">
                   <label for="keterangan" class="col-sm-2 control-label">Keterangan</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'keterangan',
                                 'id'           => 'keterangan',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Keterangan',
                                 'maxlength'=>'2'
                                 ),
                                 set_value('keterangan',$tbl_noreg_sapi['keterangan'])
                           );             
                  ?>
                 <?php echo form_error('keterangan');?>
                </div>
              </div> <!--/ Keterangan -->
               
           
      </div> <!--/ Panel Body -->
    <div class="panel-footer">   
          <div class="row"> 
              <!-- <div class="col-md-10 col-sm-12 col-md-offset-2 col-sm-offset-0"> -->
              <div class="col-md-12 col-sm-12 col-lg-12 text-right">

                   <a href="<?php echo site_url('tbl_noreg_sapi'); ?>" class="btn btn-default">
                       <i class="glyphicon glyphicon-chevron-left"></i> Kembali
                   </a> 
                    <button type="submit" class="btn btn-primary" name="post">
                        <i class="glyphicon glyphicon-floppy-save"></i> Simpan 
                    </button>                  
              </div>
          </div>
    </div><!--/ Panel Footer -->       
</div><!--/ Panel -->
<?php echo form_close(); ?>  