<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tbl_noreg_sapi
 * @created on : Tuesday, 09-Oct-2018 10:41:58
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2018    
 */
 
 
class Tbl_noreg_sapis extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }


    /**
     *  Get All data tbl_noreg_sapi
     *
     *  @param limit  : Integer
     *  @param offset : Integer
     *
     *  @return array
     *
     */
    public function get_all() 
    {

        $result = $this->db->get('tbl_noreg_sapi');

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    

    /**
     *  Count All tbl_noreg_sapi
     *    
     *  @return Integer
     *
     */
    public function count_all()
    {
        $this->db->from('tbl_noreg_sapi');
        return $this->db->count_all_results();
    }
    

    /**
    * Search All tbl_noreg_sapi
    *
    *  @param limit   : Integer
    *  @param offset  : Integer
    *  @param keyword : mixed
    *
    *  @return array
    *
    */
    public function get_search($limit, $offset) 
    {
        $keyword = $this->session->userdata('keyword');
                
        $this->db->like('id_reg', $keyword);  
                
        $this->db->like('kelas', $keyword);  
                
        $this->db->like('gambar', $keyword);  
        
        $this->db->limit($limit, $offset);
        $result = $this->db->get('tbl_noreg_sapi');

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    
    
    /**
    * Search All tbl_noreg_sapi
    * @param keyword : mixed
    *
    * @return Integer
    *
    */
    public function count_all_search()
    {
        $keyword = $this->session->userdata('keyword');
        $this->db->from('tbl_noreg_sapi');        
                
        $this->db->like('id_reg', $keyword);  
                
        $this->db->like('kelas', $keyword);  
                
        $this->db->like('gambar', $keyword);  
        
        return $this->db->count_all_results();
    }


    
    
    
    /**
    *  Get One tbl_noreg_sapi
    *
    *  @param id : Integer
    *
    *  @return array
    *
    */
    public function get_one($id) 
    {
        $this->db->where('id_reg', $id);
        $result = $this->db->get('tbl_noreg_sapi');

        if ($result->num_rows() == 1) 
        {
            return $result->row_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    /**
    *  Default form data tbl_noreg_sapi
    *  @return array
    *
    */
    public function add()
    {
        $data = array(
            
                'kelas' => '',
            
                'harga_dasar' => '',
            
                'gambar' => '',
            
                'status' => '',
            
                'keterangan' => '',
            
        );

        return $data;
    }

    
    
    
    
    /**
    *  Save data Post
    *
    *  @return void
    *
    */
    public function save() 
    {
        $data = array(
        
            'kelas' => strip_tags($this->input->post('kelas', TRUE)),
        
            'harga_dasar' => strip_tags($this->input->post('harga_dasar', TRUE)),
        
            'gambar' => strip_tags($this->input->post('gambar', TRUE)),
        
            'status' => strip_tags($this->input->post('status', TRUE)),
        
            'keterangan' => strip_tags($this->input->post('keterangan', TRUE)),
        
        );
        
        
        $this->db->insert('tbl_noreg_sapi', $data);
    }
    
    
    

    
    /**
    *  Update modify data
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function update($id)
    {
        $data = array(
        
                'kelas' => strip_tags($this->input->post('kelas', TRUE)),
        
                'harga_dasar' => strip_tags($this->input->post('harga_dasar', TRUE)),
        
                'gambar' => strip_tags($this->input->post('gambar', TRUE)),
        
                'status' => strip_tags($this->input->post('status', TRUE)),
        
                'keterangan' => strip_tags($this->input->post('keterangan', TRUE)),
        
        );
        
        
        $this->db->where('id_reg', $id);
        $this->db->update('tbl_noreg_sapi', $data);
    }


    
    
    
    /**
    *  Delete data by id
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function destroy($id)
    {       
        $this->db->where('id_reg', $id);
        $this->db->delete('tbl_noreg_sapi');
        
    }

    function get_kode()   { 
       $this->db->select('RIGHT(tbl_noreg_sapi.id_reg,3) as kode', FALSE);
          $this->db->order_by('id_reg','DESC');    
          $this->db->limit(1);     
          $query = $this->db->get('tbl_noreg_sapi');      //cek dulu apakah ada sudah ada kode di tabel.    
          if($query->num_rows() <> 0){       
           //jika kode ternyata sudah ada.      
           $data = $query->row();      
           $kode = intval($data->kode) + 1;     
          }
          else{       
           //jika kode belum ada      
           $kode = 1;     
          }
          $kodemax = str_pad($kode, 3, "00", STR_PAD_LEFT);    
          $kodejadi = "S".$kodemax;     
          return $kodejadi;  
     } 







    



}
