
<h2>Form Data sapi</h2>

<?php 
    echo $this->session->flashdata('notify');
?>


<!-- <div class="row">
    <div class="col-lg-12 col-md-12">        
        <?php 
                
                echo create_breadcrumb();        
                echo $this->session->flashdata('notify');
                
    ?>
    </div>
</div> -->

<?php echo form_open(site_url('tbl_noreg_sapi/' . $action),'role="form"  enctype="multipart/form-data" class="form-horizontal" id="form_tbl_noreg_sapi" parsley-validate'); ?>               
<div class="panel panel-default">
    <div class="panel-heading"><i class="glyphicon glyphicon-edit"></i> </div>
     
      <div class="panel-body">
         
                       
               <div class="form-group">
                   <!-- <label for="kelas" class="col-sm-2 control-label">No Reg</label> -->
                <div class="col-sm-6">                                   
                   <input type="hidden" name="kode" class="form-control" readonly="" value="<?php echo $get; ?>" >
                </div>
              </div> <!--/ ID --> 

              <div class="form-group">
                   <label for="kelas" class="col-sm-2 control-label">Kelas</label>
                <div class="col-sm-2">                                   
                   <select class="form-control pilih" name="kelas">
                      <option value=""></option>
                      <option value="A">A</option>
                      <option value="B">B</option>
                      <option value="C">C</option>
                      <option value="D">D</option>
                      <option value="S1">Super 1</option>
                      <option value="S2">Super 2</option>
                    </select>
                </div>
              </div> <!--/ Kelas -->
                          
               <div class="form-group">
                   <label for="harga_dasar" class="col-sm-2 control-label">Harga Dasar</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'harga_dasar',
                                 'id'           => 'harga_dasar',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Harga Dasar',
                                 'maxlength'=>'11'
                                 ),
                                 set_value('harga_dasar',$tbl_noreg_sapi['harga_dasar'])
                           );             
                  ?>
                 <?php echo form_error('harga_dasar');?>
                </div>
              </div> <!--/ Harga Dasar -->
                          
               <div class="form-group">
                   <label for="gambar" class="col-sm-2 control-label">Gambar</label>

                <div class="col-sm-6">
                  <?php
                   echo form_upload(
                                array(
                                 'name'         => 'gambar[]',
                                 'id'           => 'gambar',
                                 'style'        => 'left: -182.667px; top: 20px;',
                                 'title'         => 'Pilih File.....'
                                 )

                           );
                  ?>
                 <?php echo form_error('gambar');?>
                </div>
              </div> <!--/ gambar -->
                          
               <div class="form-group">
                   <label for="status" class="col-sm-2 control-label">Status</label>
               <div class="col-sm-2">                                   
                   <select class="form-control pilih" name="status">
                      <option value="1">Sehat</option>
                      <option value="2">Mati</option>
                    </select>
                </div>
              </div> <!--/ Status -->
                          
               <div class="form-group">
                   <label for="keterangan" class="col-sm-2 control-label">Keterangan</label>
                 <div class="col-sm-2">                                   
                   <select class="form-control pilih" name="keterangan">
                      <option value="1">Tersedian</option>
                      <option value="2">Terjual</option>
                    </select>
                </div>
              </div> <!--/ Keterangan -->
               
           
      </div> <!--/ Panel Body -->
    <div class="panel-footer">   
          <div class="row"> 
              <!-- <div class="col-md-10 col-sm-12 col-md-offset-2 col-sm-offset-0"> -->
              <div class="col-md-12 col-sm-12 col-lg-12 text-right">

                   <a href="<?php echo site_url('tbl_noreg_sapi'); ?>" class="btn btn-default">
                       <i class="glyphicon glyphicon-chevron-left"></i> Kembali
                   </a> 
                    <button type="submit" class="btn btn-primary" name="post">
                        <i class="glyphicon glyphicon-floppy-save"></i> Simpan 
                    </button>                  
              </div>
          </div>
    </div><!--/ Panel Footer -->       
</div><!--/ Panel -->
<?php echo form_close(); ?>  