<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tbl_noreg_sapi
 * @created on : Tuesday, 09-Oct-2018 10:41:58
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * @editor Jovin <hijovin@gmail.com>
 * Copyright 2018
 */

class Tbl_noreg_sapi extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();         
        $this->load->model('tbl_noreg_sapis');
    }
    

    /**
    * List all data tbl_noreg_sapi
    *
    */
    public function index() 
    {
        $config = array(
            'base_url'          => site_url('tbl_noreg_sapi/index/'),
            'total_rows'        => $this->tbl_noreg_sapis->count_all(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
            
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['pagination']     = $this->pagination->create_links();
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['tbl_noreg_sapis']       = $this->tbl_noreg_sapis->get_all($config['per_page'], $this->uri->segment(3));
        $this->template->render('tbl_noreg_sapi/view',$data);
	      
    }

    

    /**
    * Call Form to Add  New tbl_noreg_sapi
    *
    */
    public function add() 
    {       
        $data['tbl_noreg_sapi'] = $this->tbl_noreg_sapis->add();
        $data['get'] = $this->tbl_noreg_sapis->get_kode();

        $data['action']  = 'tbl_noreg_sapi/save';
        
        $this->template->js_add('
                $(document).ready(function(){
                // binds form submission and fields to the validation engine
                $("#form_tbl_noreg_sapi").parsley();
                        });','embed');
      
        $this->template->render('tbl_noreg_sapi/form',$data);
    }

    /**
    * Call Form to Modify tbl_noreg_sapi
    *
    */
    public function edit($id='') 
    {
        if ($id != '') 
        {

            $data['tbl_noreg_sapi']      = $this->tbl_noreg_sapis->get_one($id);
            $data['get'] = $this->tbl_noreg_sapis->get_kode();

            $data['action']       = 'tbl_noreg_sapi/save/' . $id;           
      
            $this->template->js_add('
                     $(document).ready(function(){
                    // binds form submission and fields to the validation engine
                    $("#form_tbl_noreg_sapi").parsley();
                                    });','embed');
            
            $this->template->render('tbl_noreg_sapi/form',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notify', notify('Data tidak ditemukan','info'));
            redirect(site_url('tbl_noreg_sapi'));
        }
    }


    
    /**
    * Save & Update data  tbl_noreg_sapi
    *
    */
    public function save($id =NULL) 
    {
        // validation config
        $config = array(
                  
                    array(
                        'field' => 'kelas',
                        'label' => 'Kelas',
                        'rules' => 'trim'
                        ),
                    
                    array(
                        'field' => 'harga_dasar',
                        'label' => 'Harga Dasar',
                        'rules' => 'trim'
                        ),
                    
                    array(
                        'field' => 'gambar',
                        'label' => 'Gambar',
                        'rules' => 'trim'
                        ),
                    
                    array(
                        'field' => 'status',
                        'label' => 'Status',
                        'rules' => 'trim'
                        ),
                    
                    array(
                        'field' => 'keterangan',
                        'label' => 'Keterangan',
                        'rules' => 'trim'
                        ),
                               
                  );
            
        // if id NULL then add new data
        if(!$id)
        {    
                  $this->form_validation->set_rules($config);

                  if ($this->form_validation->run() == TRUE) 
                  {
                      if ($this->input->post()) 
                      {
                          
                          // $this->tbl_noreg_sapis->save();
                          $data['kode']=$this->input->post('kode');
                          $data['kelas']=$this->input->post('kelas');
                          $data['harga_dasar']=$this->input->post('harga_dasar');
                          $data['status']=$this->input->post('status');
                          $data['keterangan']=$this->input->post('keterangan');
                          $this->do_upload2($this->input->post('kode'),$data,'1');
                          $this->session->set_flashdata('notify', notify('Data berhasil ditambahkan','success'));
                          redirect('tbl_noreg_sapi');
                      }
                  } 
                  else // If validation incorrect 
                  {
                      $this->add();
                  }
         }
         else // Update data if Form Edit send Post and ID available
         {               
                $this->form_validation->set_rules($config);

                if ($this->form_validation->run() == TRUE) 
                {
                    if ($this->input->post()) 
                    {
                        // $this->tbl_noreg_sapis->update($id);
                        $data['kelas']=$this->input->post('kelas');
                        $data['harga_dasar']=$this->input->post('harga_dasar');
                        $data['status']=$this->input->post('status');
                        $data['keterangan']=$this->input->post('keterangan');
                        $this->do_upload2($id,$data,'2');
                        $this->session->set_flashdata('notify', notify('Data berhasil diupdate','success'));
                        redirect('tbl_noreg_sapi');
                    }
                } 
                else // If validation incorrect 
                {
                    $this->edit($id);
                }
         }
    }
    
    /**
    * Detail tbl_noreg_sapi
    *
    */
    public function show($id='') 
    {
        if ($id != '') 
        {
            $data['tbl_noreg_sapi'] = $this->tbl_noreg_sapis->get_one($id);            
            $this->template->render('tbl_noreg_sapi/show',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notify', notify('Data tidak ditemukan','info'));
            redirect(site_url('tbl_noreg_sapi'));
        }
    }
    
    /**
    * Search tbl_noreg_sapi like ""
    *
    */   
    public function search()
    {
        if($this->input->post('q'))
        {
            $keyword = $this->input->post('q');
            
            $this->session->set_userdata(
                        array('keyword' => $this->input->post('q',TRUE))
                    );
        }
        
         $config = array(
            'base_url'          => site_url('tbl_noreg_sapi/search/'),
            'total_rows'        => $this->tbl_noreg_sapis->count_all_search(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['pagination']     = $this->pagination->create_links();
        $data['tbl_noreg_sapis']       = $this->tbl_noreg_sapis->get_search($config['per_page'], $this->uri->segment(3));
       
        $this->template->render('tbl_noreg_sapi/view',$data);
    }
    
    /**
    * Delete tbl_noreg_sapi by ID
    *
    */
    public function destroy($id) 
    {        
        // Agar tabel dengan ID 0 bisa terhapus
        if ($id>=0) 
        {
            $this->tbl_noreg_sapis->destroy($id);           
            $this->session->set_flashdata('notify', notify('Data berhasil dihapus','success'));
            redirect('tbl_noreg_sapi');
        }

    }

     // New Code 
     function do_upload2($kode,$data,$ket) {


         $this->load->library('upload');

         $files = $_FILES;
         $cpt = count($_FILES['gambar']['name']);
         for($i=0; $i<$cpt; $i++)
         {
             $_FILES['gambar']['name']= $files['gambar']['name'][$i];
             $_FILES['gambar']['type']= $files['gambar']['type'][$i];
             $_FILES['gambar']['tmp_name']= $files['gambar']['tmp_name'][$i];
             //$_FILES['file']['error']= $files['file']['error'][$i];
             //$_FILES['file']['size']= $files['file']['size'][$i];

         $targetPath = './assets/img/sapi/';
         $fileName=$_FILES['gambar']['name'];
         $type= $_FILES['gambar']['type']; 
         $targetFile = $targetPath . $fileName ;
         $tempFile= $_FILES['gambar']['tmp_name'];
         move_uploaded_file($tempFile, $targetFile);

         if($ket=='1'){
           $this->db->insert('tbl_noreg_sapi',
               array(
                 'id_reg'=>$kode,
                 'kelas'=>$data['kelas'],
                 'harga_dasar'=>$data['harga_dasar'],
                 'status'=>$data['status'],
                 'keterangan'=>$data['keterangan'],
                 'gambar'=>$fileName));
          }else{
            $this->db->where('id_reg',$kode);
            $this->db->update('tbl_noreg_sapi',
                array(
                  'kelas'=>$data['kelas'],
                 'harga_dasar'=>$data['harga_dasar'],
                 'status'=>$data['status'],
                 'keterangan'=>$data['keterangan'],
                  'gambar'=>$fileName));
          }


        }
     }


}
?>