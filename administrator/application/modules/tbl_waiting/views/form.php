
<h2>Tbl Waiting</h2>

<?php 
    echo $this->session->flashdata('notify');
?>


<!-- <div class="row">
    <div class="col-lg-12 col-md-12">        
        <?php 
                
                echo create_breadcrumb();        
                echo $this->session->flashdata('notify');
                
    ?>
    </div>
</div> -->

<?php echo form_open(site_url('tbl_waiting/' . $action),'role="form" class="form-horizontal" id="form_tbl_waiting" parsley-validate'); ?>               
<div class="panel panel-default">
    <div class="panel-heading"><i class="glyphicon glyphicon-edit"></i> </div>
     
      <div class="panel-body">
         
                       
               <div class="form-group">
                   <label for="nama_pemesan" class="col-sm-2 control-label">Nama Pemesan</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'nama_pemesan',
                                 'id'           => 'nama_pemesan',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Nama Pemesan',
                                 'maxlength'=>'50'
                                 ),
                                 set_value('nama_pemesan',$tbl_waiting['nama_pemesan'])
                           );             
                  ?>
                 <?php echo form_error('nama_pemesan');?>
                </div>
              </div> <!--/ Nama Pemesan -->
               
           
      </div> <!--/ Panel Body -->
    <div class="panel-footer">   
          <div class="row"> 
              <!-- <div class="col-md-10 col-sm-12 col-md-offset-2 col-sm-offset-0"> -->
              <div class="col-md-12 col-sm-12 col-lg-12 text-right">

                   <a href="<?php echo site_url('tbl_waiting'); ?>" class="btn btn-default">
                       <i class="glyphicon glyphicon-chevron-left"></i> Kembali
                   </a> 
                    <button type="submit" class="btn btn-primary" name="post">
                        <i class="glyphicon glyphicon-floppy-save"></i> Simpan 
                    </button>                  
              </div>
          </div>
    </div><!--/ Panel Footer -->       
</div><!--/ Panel -->
<?php echo form_close(); ?>  