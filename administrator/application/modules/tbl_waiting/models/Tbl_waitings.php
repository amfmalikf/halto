<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tbl_waiting
 * @created on : Tuesday, 09-Oct-2018 10:42:51
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2018    
 */
 
 
class Tbl_waitings extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }


    /**
     *  Get All data tbl_waiting
     *
     *  @param limit  : Integer
     *  @param offset : Integer
     *
     *  @return array
     *
     */
    public function get_all($limit, $offset) 
    {

        $result = $this->db->get('tbl_waiting', $limit, $offset);

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    

    /**
     *  Count All tbl_waiting
     *    
     *  @return Integer
     *
     */
    public function count_all()
    {
        $this->db->from('tbl_waiting');
        return $this->db->count_all_results();
    }
    

    /**
    * Search All tbl_waiting
    *
    *  @param limit   : Integer
    *  @param offset  : Integer
    *  @param keyword : mixed
    *
    *  @return array
    *
    */
    public function get_search($limit, $offset) 
    {
        $keyword = $this->session->userdata('keyword');
                
        $this->db->like('nama_pemesan', $keyword);  
        
        $this->db->limit($limit, $offset);
        $result = $this->db->get('tbl_waiting');

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    
    
    /**
    * Search All tbl_waiting
    * @param keyword : mixed
    *
    * @return Integer
    *
    */
    public function count_all_search()
    {
        $keyword = $this->session->userdata('keyword');
        $this->db->from('tbl_waiting');        
                
        $this->db->like('nama_pemesan', $keyword);  
        
        return $this->db->count_all_results();
    }


    
    
    
    /**
    *  Get One tbl_waiting
    *
    *  @param id : Integer
    *
    *  @return array
    *
    */
    public function get_one($id) 
    {
        $this->db->where('id_waiting', $id);
        $result = $this->db->get('tbl_waiting');

        if ($result->num_rows() == 1) 
        {
            return $result->row_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    /**
    *  Default form data tbl_waiting
    *  @return array
    *
    */
    public function add()
    {
        $data = array(
            
                'nama_pemesan' => '',
            
        );

        return $data;
    }

    
    
    
    
    /**
    *  Save data Post
    *
    *  @return void
    *
    */
    public function save() 
    {
        $data = array(
        
            'nama_pemesan' => strip_tags($this->input->post('nama_pemesan', TRUE)),
        
        );
        
        
        $this->db->insert('tbl_waiting', $data);
    }
    
    
    

    
    /**
    *  Update modify data
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function update($id)
    {
        $data = array(
        
                'nama_pemesan' => strip_tags($this->input->post('nama_pemesan', TRUE)),
        
        );
        
        
        $this->db->where('id_waiting', $id);
        $this->db->update('tbl_waiting', $data);
    }


    
    
    
    /**
    *  Delete data by id
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function destroy($id)
    {       
        $this->db->where('id_waiting', $id);
        $this->db->delete('tbl_waiting');
        
    }







    



}
