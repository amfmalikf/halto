<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tbl_transaksi
 * @created on : Tuesday, 09-Oct-2018 10:42:17
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2018    
 */
 
 
class Tbl_transaksis extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }


    /**
     *  Get All data tbl_transaksi
     *
     *  @param limit  : Integer
     *  @param offset : Integer
     *
     *  @return array
     *
     */
    public function get_all($limit, $offset) 
    {

        $result = $this->db->get('tbl_transaksi', $limit, $offset);

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    

    /**
     *  Count All tbl_transaksi
     *    
     *  @return Integer
     *
     */
    public function count_all()
    {
        $this->db->from('tbl_transaksi');
        return $this->db->count_all_results();
    }
    

    /**
    * Search All tbl_transaksi
    *
    *  @param limit   : Integer
    *  @param offset  : Integer
    *  @param keyword : mixed
    *
    *  @return array
    *
    */
    public function get_search($limit, $offset) 
    {
        $keyword = $this->session->userdata('keyword');
                
        $this->db->like('id_reg', $keyword);  
        
        $this->db->limit($limit, $offset);
        $result = $this->db->get('tbl_transaksi');

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    
    
    /**
    * Search All tbl_transaksi
    * @param keyword : mixed
    *
    * @return Integer
    *
    */
    public function count_all_search()
    {
        $keyword = $this->session->userdata('keyword');
        $this->db->from('tbl_transaksi');        
                
        $this->db->like('id_reg', $keyword);  
        
        return $this->db->count_all_results();
    }


    
    
    
    /**
    *  Get One tbl_transaksi
    *
    *  @param id : Integer
    *
    *  @return array
    *
    */
    public function get_one($id) 
    {
        $this->db->where('id_transaksi', $id);
        $result = $this->db->get('tbl_transaksi');

        if ($result->num_rows() == 1) 
        {
            return $result->row_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    /**
    *  Default form data tbl_transaksi
    *  @return array
    *
    */
    public function add()
    {
        $data = array(
            
                'tgl' => '',
            
                'harga_jual' => '',
            
                'id_user' => '',
            
                'id_reg' => '',
            
        );

        return $data;
    }

    
    
    
    
    /**
    *  Save data Post
    *
    *  @return void
    *
    */
    public function save() 
    {
        $data = array(
        
            'tgl' => strip_tags($this->input->post('tgl', TRUE)),
        
            'harga_jual' => strip_tags($this->input->post('harga_jual', TRUE)),
        
            'id_user' => strip_tags($this->input->post('id_user', TRUE)),
        
            'id_reg' => strip_tags($this->input->post('id_reg', TRUE)),
        
        );
        
        
        $this->db->insert('tbl_transaksi', $data);
    }
    
    
    

    
    /**
    *  Update modify data
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function update($id)
    {
        $data = array(
        
                'tgl' => strip_tags($this->input->post('tgl', TRUE)),
        
                'harga_jual' => strip_tags($this->input->post('harga_jual', TRUE)),
        
                'id_user' => strip_tags($this->input->post('id_user', TRUE)),
        
                'id_reg' => strip_tags($this->input->post('id_reg', TRUE)),
        
        );
        
        
        $this->db->where('id_transaksi', $id);
        $this->db->update('tbl_transaksi', $data);
    }


    
    
    
    /**
    *  Delete data by id
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function destroy($id)
    {       
        $this->db->where('id_transaksi', $id);
        $this->db->delete('tbl_transaksi');
        
    }







    



}
