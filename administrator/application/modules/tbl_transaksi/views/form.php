
<h2>Tbl Transaksi</h2>

<?php 
    echo $this->session->flashdata('notify');
?>


<!-- <div class="row">
    <div class="col-lg-12 col-md-12">        
        <?php 
                
                echo create_breadcrumb();        
                echo $this->session->flashdata('notify');
                
    ?>
    </div>
</div> -->

<?php echo form_open(site_url('tbl_transaksi/' . $action),'role="form" class="form-horizontal" id="form_tbl_transaksi" parsley-validate'); ?>               
<div class="panel panel-default">
    <div class="panel-heading"><i class="glyphicon glyphicon-edit"></i> </div>
     
      <div class="panel-body">
         
                       
               <div class="form-group">
                   <label for="tgl" class="col-sm-2 control-label">Tgl</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'tgl',
                                 'id'           => 'tgl',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Tgl',
                                 
                                 ),
                                 set_value('tgl',$tbl_transaksi['tgl'])
                           );             
                  ?>
                 <?php echo form_error('tgl');?>
                </div>
              </div> <!--/ Tgl -->
                          
               <div class="form-group">
                   <label for="harga_jual" class="col-sm-2 control-label">Harga Jual</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'harga_jual',
                                 'id'           => 'harga_jual',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Harga Jual',
                                 'maxlength'=>'11'
                                 ),
                                 set_value('harga_jual',$tbl_transaksi['harga_jual'])
                           );             
                  ?>
                 <?php echo form_error('harga_jual');?>
                </div>
              </div> <!--/ Harga Jual -->
                          
               <div class="form-group">
                   <label for="id_user" class="col-sm-2 control-label">Id User</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'id_user',
                                 'id'           => 'id_user',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Id User',
                                 'maxlength'=>'11'
                                 ),
                                 set_value('id_user',$tbl_transaksi['id_user'])
                           );             
                  ?>
                 <?php echo form_error('id_user');?>
                </div>
              </div> <!--/ Id User -->
                          
               <div class="form-group">
                   <label for="id_reg" class="col-sm-2 control-label">Id Reg</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'id_reg',
                                 'id'           => 'id_reg',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Id Reg',
                                 'maxlength'=>'4'
                                 ),
                                 set_value('id_reg',$tbl_transaksi['id_reg'])
                           );             
                  ?>
                 <?php echo form_error('id_reg');?>
                </div>
              </div> <!--/ Id Reg -->
               
           
      </div> <!--/ Panel Body -->
    <div class="panel-footer">   
          <div class="row"> 
              <!-- <div class="col-md-10 col-sm-12 col-md-offset-2 col-sm-offset-0"> -->
              <div class="col-md-12 col-sm-12 col-lg-12 text-right">

                   <a href="<?php echo site_url('tbl_transaksi'); ?>" class="btn btn-default">
                       <i class="glyphicon glyphicon-chevron-left"></i> Kembali
                   </a> 
                    <button type="submit" class="btn btn-primary" name="post">
                        <i class="glyphicon glyphicon-floppy-save"></i> Simpan 
                    </button>                  
              </div>
          </div>
    </div><!--/ Panel Footer -->       
</div><!--/ Panel -->
<?php echo form_close(); ?>  