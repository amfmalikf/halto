<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tbl_transaksi
 * @created on : Tuesday, 09-Oct-2018 10:42:17
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * @editor Jovin <hijovin@gmail.com>
 * Copyright 2018
 */

class Tbl_transaksi extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();         
        $this->load->model('tbl_transaksis');
    }
    

    /**
    * List all data tbl_transaksi
    *
    */
    public function index() 
    {
        $config = array(
            'base_url'          => site_url('tbl_transaksi/index/'),
            'total_rows'        => $this->tbl_transaksis->count_all(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
            
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['pagination']     = $this->pagination->create_links();
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['tbl_transaksis']       = $this->tbl_transaksis->get_all($config['per_page'], $this->uri->segment(3));
        $this->template->render('tbl_transaksi/view',$data);
	      
    }

    

    /**
    * Call Form to Add  New tbl_transaksi
    *
    */
    public function add() 
    {       
        $data['tbl_transaksi'] = $this->tbl_transaksis->add();
        $data['action']  = 'tbl_transaksi/save';
        
        $this->template->js_add('
                $(document).ready(function(){
                // binds form submission and fields to the validation engine
                $("#form_tbl_transaksi").parsley();
                        });','embed');
      
        $this->template->render('tbl_transaksi/form',$data);
    }

    /**
    * Call Form to Modify tbl_transaksi
    *
    */
    public function edit($id='') 
    {
        if ($id != '') 
        {

            $data['tbl_transaksi']      = $this->tbl_transaksis->get_one($id);
            $data['action']       = 'tbl_transaksi/save/' . $id;           
      
            $this->template->js_add('
                     $(document).ready(function(){
                    // binds form submission and fields to the validation engine
                    $("#form_tbl_transaksi").parsley();
                                    });','embed');
            
            $this->template->render('tbl_transaksi/form',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notify', notify('Data tidak ditemukan','info'));
            redirect(site_url('tbl_transaksi'));
        }
    }


    
    /**
    * Save & Update data  tbl_transaksi
    *
    */
    public function save($id =NULL) 
    {
        // validation config
        $config = array(
                  
                    array(
                        'field' => 'tgl',
                        'label' => 'Tgl',
                        'rules' => 'trim'
                        ),
                    
                    array(
                        'field' => 'harga_jual',
                        'label' => 'Harga Jual',
                        'rules' => 'trim'
                        ),
                    
                    array(
                        'field' => 'id_user',
                        'label' => 'Id User',
                        'rules' => 'trim'
                        ),
                    
                    array(
                        'field' => 'id_reg',
                        'label' => 'Id Reg',
                        'rules' => 'trim'
                        ),
                               
                  );
            
        // if id NULL then add new data
        if(!$id)
        {    
                  $this->form_validation->set_rules($config);

                  if ($this->form_validation->run() == TRUE) 
                  {
                      if ($this->input->post()) 
                      {
                          
                          $this->tbl_transaksis->save();
                          $this->session->set_flashdata('notify', notify('Data berhasil ditambahkan','success'));
                          redirect('tbl_transaksi');
                      }
                  } 
                  else // If validation incorrect 
                  {
                      $this->add();
                  }
         }
         else // Update data if Form Edit send Post and ID available
         {               
                $this->form_validation->set_rules($config);

                if ($this->form_validation->run() == TRUE) 
                {
                    if ($this->input->post()) 
                    {
                        $this->tbl_transaksis->update($id);
                        $this->session->set_flashdata('notify', notify('Data berhasil diupdate','success'));
                        redirect('tbl_transaksi');
                    }
                } 
                else // If validation incorrect 
                {
                    $this->edit($id);
                }
         }
    }
    
    /**
    * Detail tbl_transaksi
    *
    */
    public function show($id='') 
    {
        if ($id != '') 
        {
            $data['tbl_transaksi'] = $this->tbl_transaksis->get_one($id);            
            $this->template->render('tbl_transaksi/show',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notify', notify('Data tidak ditemukan','info'));
            redirect(site_url('tbl_transaksi'));
        }
    }
    
    /**
    * Search tbl_transaksi like ""
    *
    */   
    public function search()
    {
        if($this->input->post('q'))
        {
            $keyword = $this->input->post('q');
            
            $this->session->set_userdata(
                        array('keyword' => $this->input->post('q',TRUE))
                    );
        }
        
         $config = array(
            'base_url'          => site_url('tbl_transaksi/search/'),
            'total_rows'        => $this->tbl_transaksis->count_all_search(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['pagination']     = $this->pagination->create_links();
        $data['tbl_transaksis']       = $this->tbl_transaksis->get_search($config['per_page'], $this->uri->segment(3));
       
        $this->template->render('tbl_transaksi/view',$data);
    }
    
    /**
    * Delete tbl_transaksi by ID
    *
    */
    public function destroy($id) 
    {        
        // Agar tabel dengan ID 0 bisa terhapus
        if ($id>=0) 
        {
            $this->tbl_transaksis->destroy($id);           
            $this->session->set_flashdata('notify', notify('Data berhasil dihapus','success'));
            redirect('tbl_transaksi');
        }

    }
}
?>