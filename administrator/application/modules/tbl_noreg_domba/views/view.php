
<h2>Data Domba</h2>

<?php 
    echo $this->session->flashdata('notify');
?>

<!-- <div class="row">
    <div class="col-lg-12 col-md-12">        
        <?php 
                
            echo create_breadcrumb();        
            echo $this->session->flashdata('notify');
                
        ?>
    </div>
</div> -->

<section class="panel panel-default">
    <header class="panel-heading">
        <div class="row">
            <div class="col-md-8 col-xs-3">                
                <?php
                      echo anchor(
                               site_url('tbl_noreg_domba/add'),
                                '<i class="glyphicon glyphicon-plus"></i> Tambah',
                                'class="btn btn-success btn-sm" data-tooltip="tooltip" data-placement="top" title="Tambah Data"'
                              );
                ?>
            </div>
            <!-- <div class="col-md-4 col-xs-9">
                                           
                 <?php echo form_open(site_url('tbl_noreg_domba/search'), 'role="search" class="form"') ;?>       
                           <div class="input-group pull-right">                      
                                 <input type="text" class="form-control input-sm" placeholder="Cari data" name="q" autocomplete="off"> 
                                 <span class="input-group-btn">
                                      <button class="btn btn-primary btn-sm" type="submit"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                 </span>
                           </div>
                           
               </form> 
                <?php echo form_close(); ?>
            </div> -->
        </div>
    </header>
    
    
    <div class="panel-body">
         <?php if ($tbl_noreg_dombas) : ?>
          <table  class="example1 table table-hover table-condensed">
              
            <thead>
              <tr>
                <th class="header">No</th>
                
                    <th>No Reg</th>   
                    <th>Kelas</th>   
                
                    <th>Harga Dasar</th>   
                
                    <th>Gambar</th>   
                
                    <th>Status</th>   
                
                    <th>Keterangan</th>   
                
                <th class="red header" align="right" width="120">Aksi</th>
              </tr>
            </thead>
            
            
            <tbody>
             
               <?php foreach ($tbl_noreg_dombas as $tbl_noreg_domba) : ?>
              <tr>
              	<td><?php echo $number++;; ?> </td>
               
               <td><?php echo $tbl_noreg_domba['id_reg']; ?></td>
               <td><?php echo $tbl_noreg_domba['kelas']; ?></td>
               
               <td><?php echo "Rp ".  number_format($tbl_noreg_domba['harga_dasar'],2,",",".").",-" ?></td>
               
               <td>
                   <div class="tz-gallery">
                         <a class="lightbox " href="<?php echo base_url('assets/img/domba/'.$tbl_noreg_domba['gambar']);?>">
                             <img style="width: auto; height: 100px;" data-tooltip="tooltip" title="Lihat Gambar" src="<?php echo base_url('assets/img/domba/'.$tbl_noreg_domba['gambar']);?>"  alt="Park">
                         </a>
                   </div>
               </td>
               
               <td>
                  <?php if($tbl_noreg_domba['status']=='1') {
                    echo "<span class='label label-success' style='font-size: 14px '>Sehat</span>";
                    } 
                  elseif ($tbl_noreg_domba['status']=='2') {
                      echo "<span class='label label-danger' style='font-size: 14px '>Mati</span>";
                    }
                    $tbl_noreg_domba['status'];?>
                </td>
               
               <td>
                 
                  <?php if($tbl_noreg_domba['keterangan']=='1') {
                    echo "<span class='label label-success' style='font-size: 14px '>Tersedian</span>";
                    } 
                  elseif ($tbl_noreg_domba['keterangan']=='2') {
                      echo "<span class='label label-danger' style='font-size: 14px '>Terjual</span>";
                    }
                    $tbl_noreg_domba['keterangan'];?>
               </td>
               
                <td nowrap>    
                    
                  
                    
                    <?php
                                  echo anchor(
                                          site_url('tbl_noreg_domba/edit/' . $tbl_noreg_domba['id_reg']),
                                            '<i class="glyphicon glyphicon-edit"></i>',
                                            'class="btn btn-sm btn-success" data-tooltip="tooltip" data-placement="top" title="Edit"'
                                          );
                   ?>
                   
                   <?php
                                  echo anchor(
                                          site_url('tbl_noreg_domba/destroy/' . $tbl_noreg_domba['id_reg']),
                                            '<i class="glyphicon glyphicon-trash"></i>',
                                            'onclick="return confirm(\'Apakah anda yakin ingin menghapus?\');" class="btn btn-sm btn-danger" data-tooltip="tooltip" data-placement="top" title="Hapus"'
                                          );
                   ?>   
                                 
                </td>
              </tr>     
               <?php endforeach; ?>
            </tbody>
          </table>
          <?php else: ?>
                <?php  echo notify('Data tbl_noreg_domba belum tersedia','info');?>
          <?php endif; ?>
    </div>
    
    
    <div class="panel-footer">
       <!--  <div class="row">
           <div class="col-md-3">
               Tbl Noreg Domba
               <span class="label label-info">
                    <?php echo $total; ?>
               </span>
           </div>  
           <div class="col-md-9">
                 <?php echo $pagination; ?>
           </div>
        </div> -->
    </div>
</section>