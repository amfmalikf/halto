<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tbl_detail_waiting
 * @created on : Tuesday, 09-Oct-2018 10:43:06
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * @editor Jovin <hijovin@gmail.com>
 * Copyright 2018
 */

class Tbl_detail_waiting extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();         
        $this->load->model('tbl_detail_waitings');
    }
    

    /**
    * List all data tbl_detail_waiting
    *
    */
    public function index() 
    {
        $config = array(
            'base_url'          => site_url('tbl_detail_waiting/index/'),
            'total_rows'        => $this->tbl_detail_waitings->count_all(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
            
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['pagination']     = $this->pagination->create_links();
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['tbl_detail_waitings']       = $this->tbl_detail_waitings->get_all($config['per_page'], $this->uri->segment(3));
        $this->template->render('tbl_detail_waiting/view',$data);
	      
    }

    

    /**
    * Call Form to Add  New tbl_detail_waiting
    *
    */
    public function add() 
    {       
        $data['tbl_detail_waiting'] = $this->tbl_detail_waitings->add();
        $data['action']  = 'tbl_detail_waiting/save';
        
        $this->template->js_add('
                $(document).ready(function(){
                // binds form submission and fields to the validation engine
                $("#form_tbl_detail_waiting").parsley();
                        });','embed');
      
        $this->template->render('tbl_detail_waiting/form',$data);
    }

    /**
    * Call Form to Modify tbl_detail_waiting
    *
    */
    public function edit($id='') 
    {
        if ($id != '') 
        {

            $data['tbl_detail_waiting']      = $this->tbl_detail_waitings->get_one($id);
            $data['action']       = 'tbl_detail_waiting/save/' . $id;           
      
            $this->template->js_add('
                     $(document).ready(function(){
                    // binds form submission and fields to the validation engine
                    $("#form_tbl_detail_waiting").parsley();
                                    });','embed');
            
            $this->template->render('tbl_detail_waiting/form',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notify', notify('Data tidak ditemukan','info'));
            redirect(site_url('tbl_detail_waiting'));
        }
    }


    
    /**
    * Save & Update data  tbl_detail_waiting
    *
    */
    public function save($id =NULL) 
    {
        // validation config
        $config = array(
                  
                    array(
                        'field' => 'id_waiting',
                        'label' => 'Id Waiting',
                        'rules' => 'trim'
                        ),
                    
                    array(
                        'field' => 'id_reg',
                        'label' => 'Id Reg',
                        'rules' => 'trim'
                        ),
                    
                    array(
                        'field' => 'kelas',
                        'label' => 'Kelas',
                        'rules' => 'trim'
                        ),
                               
                  );
            
        // if id NULL then add new data
        if(!$id)
        {    
                  $this->form_validation->set_rules($config);

                  if ($this->form_validation->run() == TRUE) 
                  {
                      if ($this->input->post()) 
                      {
                          
                          $this->tbl_detail_waitings->save();
                          $this->session->set_flashdata('notify', notify('Data berhasil ditambahkan','success'));
                          redirect('tbl_detail_waiting');
                      }
                  } 
                  else // If validation incorrect 
                  {
                      $this->add();
                  }
         }
         else // Update data if Form Edit send Post and ID available
         {               
                $this->form_validation->set_rules($config);

                if ($this->form_validation->run() == TRUE) 
                {
                    if ($this->input->post()) 
                    {
                        $this->tbl_detail_waitings->update($id);
                        $this->session->set_flashdata('notify', notify('Data berhasil diupdate','success'));
                        redirect('tbl_detail_waiting');
                    }
                } 
                else // If validation incorrect 
                {
                    $this->edit($id);
                }
         }
    }
    
    /**
    * Detail tbl_detail_waiting
    *
    */
    public function show($id='') 
    {
        if ($id != '') 
        {
            $data['tbl_detail_waiting'] = $this->tbl_detail_waitings->get_one($id);            
            $this->template->render('tbl_detail_waiting/show',$data);
            
        }
        else 
        {
            $this->session->set_flashdata('notify', notify('Data tidak ditemukan','info'));
            redirect(site_url('tbl_detail_waiting'));
        }
    }
    
    /**
    * Search tbl_detail_waiting like ""
    *
    */   
    public function search()
    {
        if($this->input->post('q'))
        {
            $keyword = $this->input->post('q');
            
            $this->session->set_userdata(
                        array('keyword' => $this->input->post('q',TRUE))
                    );
        }
        
         $config = array(
            'base_url'          => site_url('tbl_detail_waiting/search/'),
            'total_rows'        => $this->tbl_detail_waitings->count_all_search(),
            'per_page'          => $this->config->item('per_page'),
            'uri_segment'       => 3,
            'num_links'         => 9,
            'use_page_numbers'  => FALSE
        );
        
        $this->pagination->initialize($config);
        $data['total']          = $config['total_rows'];
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['pagination']     = $this->pagination->create_links();
        $data['tbl_detail_waitings']       = $this->tbl_detail_waitings->get_search($config['per_page'], $this->uri->segment(3));
       
        $this->template->render('tbl_detail_waiting/view',$data);
    }
    
    /**
    * Delete tbl_detail_waiting by ID
    *
    */
    public function destroy($id) 
    {        
        // Agar tabel dengan ID 0 bisa terhapus
        if ($id>=0) 
        {
            $this->tbl_detail_waitings->destroy($id);           
            $this->session->set_flashdata('notify', notify('Data berhasil dihapus','success'));
            redirect('tbl_detail_waiting');
        }

    }
}
?>