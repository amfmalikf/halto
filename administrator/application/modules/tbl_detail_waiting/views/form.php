
<h2>Tbl Detail Waiting</h2>

<?php 
    echo $this->session->flashdata('notify');
?>


<!-- <div class="row">
    <div class="col-lg-12 col-md-12">        
        <?php 
                
                echo create_breadcrumb();        
                echo $this->session->flashdata('notify');
                
    ?>
    </div>
</div> -->

<?php echo form_open(site_url('tbl_detail_waiting/' . $action),'role="form" class="form-horizontal" id="form_tbl_detail_waiting" parsley-validate'); ?>               
<div class="panel panel-default">
    <div class="panel-heading"><i class="glyphicon glyphicon-edit"></i> </div>
     
      <div class="panel-body">
         
                       
               <div class="form-group">
                   <label for="id_waiting" class="col-sm-2 control-label">Id Waiting</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'id_waiting',
                                 'id'           => 'id_waiting',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Id Waiting',
                                 'maxlength'=>'11'
                                 ),
                                 set_value('id_waiting',$tbl_detail_waiting['id_waiting'])
                           );             
                  ?>
                 <?php echo form_error('id_waiting');?>
                </div>
              </div> <!--/ Id Waiting -->
                          
               <div class="form-group">
                   <label for="id_reg" class="col-sm-2 control-label">Id Reg</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'id_reg',
                                 'id'           => 'id_reg',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Id Reg',
                                 'maxlength'=>'4'
                                 ),
                                 set_value('id_reg',$tbl_detail_waiting['id_reg'])
                           );             
                  ?>
                 <?php echo form_error('id_reg');?>
                </div>
              </div> <!--/ Id Reg -->
                          
               <div class="form-group">
                   <label for="kelas" class="col-sm-2 control-label">Kelas</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'kelas',
                                 'id'           => 'kelas',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Kelas',
                                 'maxlength'=>'1'
                                 ),
                                 set_value('kelas',$tbl_detail_waiting['kelas'])
                           );             
                  ?>
                 <?php echo form_error('kelas');?>
                </div>
              </div> <!--/ Kelas -->
               
           
      </div> <!--/ Panel Body -->
    <div class="panel-footer">   
          <div class="row"> 
              <!-- <div class="col-md-10 col-sm-12 col-md-offset-2 col-sm-offset-0"> -->
              <div class="col-md-12 col-sm-12 col-lg-12 text-right">

                   <a href="<?php echo site_url('tbl_detail_waiting'); ?>" class="btn btn-default">
                       <i class="glyphicon glyphicon-chevron-left"></i> Kembali
                   </a> 
                    <button type="submit" class="btn btn-primary" name="post">
                        <i class="glyphicon glyphicon-floppy-save"></i> Simpan 
                    </button>                  
              </div>
          </div>
    </div><!--/ Panel Footer -->       
</div><!--/ Panel -->
<?php echo form_close(); ?>  