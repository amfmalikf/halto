
<h2>Tbl User</h2>

<?php 
    echo $this->session->flashdata('notify');
?>


<!-- <div class="row">
    <div class="col-lg-12 col-md-12">        
        <?php 
                
                echo create_breadcrumb();        
                echo $this->session->flashdata('notify');
                
    ?>
    </div>
</div> -->

<?php echo form_open(site_url('tbl_user/' . $action),'role="form" class="form-horizontal" id="form_tbl_user" parsley-validate'); ?>               
<div class="panel panel-default">
    <div class="panel-heading"><i class="glyphicon glyphicon-edit"></i> </div>
     
      <div class="panel-body">
         
                       
               <div class="form-group">
                   <label for="nama" class="col-sm-2 control-label">Nama</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'nama',
                                 'id'           => 'nama',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Nama',
                                 'maxlength'=>'25'
                                 ),
                                 set_value('nama',$tbl_user['nama'])
                           );             
                  ?>
                 <?php echo form_error('nama');?>
                </div>
              </div> <!--/ Nama -->
                          
               <div class="form-group">
                   <label for="password" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'password',
                                 'id'           => 'password',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Password',
                                 'maxlength'=>'25'
                                 ),
                                 set_value('password',$tbl_user['password'])
                           );             
                  ?>
                 <?php echo form_error('password');?>
                </div>
              </div> <!--/ Password -->
                          
               <div class="form-group">
                   <label for="posisi" class="col-sm-2 control-label">Posisi</label>
                <div class="col-sm-6">                                   
                  <?php                  
                   echo form_input(
                                array(
                                 'name'         => 'posisi',
                                 'id'           => 'posisi',                       
                                 'class'        => 'form-control input-sm ',
                                 'placeholder'  => 'Posisi',
                                 'maxlength'=>'2'
                                 ),
                                 set_value('posisi',$tbl_user['posisi'])
                           );             
                  ?>
                 <?php echo form_error('posisi');?>
                </div>
              </div> <!--/ Posisi -->
               
           
      </div> <!--/ Panel Body -->
    <div class="panel-footer">   
          <div class="row"> 
              <!-- <div class="col-md-10 col-sm-12 col-md-offset-2 col-sm-offset-0"> -->
              <div class="col-md-12 col-sm-12 col-lg-12 text-right">

                   <a href="<?php echo site_url('tbl_user'); ?>" class="btn btn-default">
                       <i class="glyphicon glyphicon-chevron-left"></i> Kembali
                   </a> 
                    <button type="submit" class="btn btn-primary" name="post">
                        <i class="glyphicon glyphicon-floppy-save"></i> Simpan 
                    </button>                  
              </div>
          </div>
    </div><!--/ Panel Footer -->       
</div><!--/ Panel -->
<?php echo form_close(); ?>  