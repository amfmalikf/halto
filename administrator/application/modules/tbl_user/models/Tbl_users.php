<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tbl_user
 * @created on : Tuesday, 09-Oct-2018 10:41:45
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2018    
 */
 
 
class Tbl_users extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }


    /**
     *  Get All data tbl_user
     *
     *  @param limit  : Integer
     *  @param offset : Integer
     *
     *  @return array
     *
     */
    public function get_all($limit, $offset) 
    {

        $result = $this->db->get('tbl_user', $limit, $offset);

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    

    /**
     *  Count All tbl_user
     *    
     *  @return Integer
     *
     */
    public function count_all()
    {
        $this->db->from('tbl_user');
        return $this->db->count_all_results();
    }
    

    /**
    * Search All tbl_user
    *
    *  @param limit   : Integer
    *  @param offset  : Integer
    *  @param keyword : mixed
    *
    *  @return array
    *
    */
    public function get_search($limit, $offset) 
    {
        $keyword = $this->session->userdata('keyword');
                
        $this->db->like('nama', $keyword);  
                
        $this->db->like('password', $keyword);  
        
        $this->db->limit($limit, $offset);
        $result = $this->db->get('tbl_user');

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    
    
    /**
    * Search All tbl_user
    * @param keyword : mixed
    *
    * @return Integer
    *
    */
    public function count_all_search()
    {
        $keyword = $this->session->userdata('keyword');
        $this->db->from('tbl_user');        
                
        $this->db->like('nama', $keyword);  
                
        $this->db->like('password', $keyword);  
        
        return $this->db->count_all_results();
    }


    
    
    
    /**
    *  Get One tbl_user
    *
    *  @param id : Integer
    *
    *  @return array
    *
    */
    public function get_one($id) 
    {
        $this->db->where('id_user', $id);
        $result = $this->db->get('tbl_user');

        if ($result->num_rows() == 1) 
        {
            return $result->row_array();
        } 
        else 
        {
            return array();
        }
    }

    
    
    
    /**
    *  Default form data tbl_user
    *  @return array
    *
    */
    public function add()
    {
        $data = array(
            
                'nama' => '',
            
                'password' => '',
            
                'posisi' => '',
            
        );

        return $data;
    }

    
    
    
    
    /**
    *  Save data Post
    *
    *  @return void
    *
    */
    public function save() 
    {
        $data = array(
        
            'nama' => strip_tags($this->input->post('nama', TRUE)),
        
            'password' => strip_tags($this->input->post('password', TRUE)),
        
            'posisi' => strip_tags($this->input->post('posisi', TRUE)),
        
        );
        
        
        $this->db->insert('tbl_user', $data);
    }
    
    
    

    
    /**
    *  Update modify data
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function update($id)
    {
        $data = array(
        
                'nama' => strip_tags($this->input->post('nama', TRUE)),
        
                'password' => strip_tags($this->input->post('password', TRUE)),
        
                'posisi' => strip_tags($this->input->post('posisi', TRUE)),
        
        );
        
        
        $this->db->where('id_user', $id);
        $this->db->update('tbl_user', $data);
    }


    
    
    
    /**
    *  Delete data by id
    *
    *  @param id : Integer
    *
    *  @return void
    *
    */
    public function destroy($id)
    {       
        $this->db->where('id_user', $id);
        $this->db->delete('tbl_user');
        
    }







    



}
