<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
      <div class="user-panel">
            <div class="pull-left image"> 
                <img src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/dist/img/user2-160x160.jpg') ?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p><?php echo $this->session->userdata('namalengkap');?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form --><!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
            <li class="header">MENU APLIKASI</li>
        <?php if ($this->session->userdata('akses') == "1") { ?>
            
            <li class="treeview">
                <a href="<?php echo site_url('dashboard');?>">
            <i class="fa fa-dashboard"></i> <span>Home</span></a></li>
             <li><a href="<?php echo site_url('tbl_user');?>"><i class="fa fa-angle-double-down"></i>Data Master User</a></li>
                    <li><a href="<?php echo site_url('tbl_noreg_domba');?>"><i class="fa fa-angle-double-down"></i>Data Master Domba</a></li>
                    <li><a href="<?php echo site_url('tbl_noreg_sapi');?>"><i class="fa fa-angle-double-down"></i>Data Master Sapi</a></li>
                    <li><a href="<?php echo site_url('tbl_transaksi');?>"><i class="fa fa-angle-double-down"></i>Data Transaksi</a></li>

            
            <li class="treeview">
                <a href="<?php echo site_url('login/logout');?>">
            <i class="fa fa-arrow-left"></i> <span>Logout</span></a></li>
      <?php } ?>

      </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">