<?php

class global_m extends CI_Model{    

    function get_employee($id)
	{
	   $this->db->select('id_employee');
	   $this->db->from('users');
	   $this->db->where('id', $id);
	   $res = $this->db->get();
	   if($res->num_rows()>0)
	   {
	     $row = $res->row();
	     return $row->id_employee;
	   }
	   else
	   {
	     return '';
	   }
	}

	function get_one_field($field, $table, $where, $params)
	{
	 	$this->db->select($field);
	   	$this->db->from($table);
	   	$this->db->where($where, $params);
	   	$res = $this->db->get();
	   	if($res->num_rows()>0)
	   	{
	    	$row = $res->row();
	     	return $row->$field;
	   	}
	   	else
	   	{
	     	return '';
	   	}	
	}
	
	function get_one_field1($field, $table, $where, $params,$where1,$params1)
	{
	 	$this->db->select($field);
	   	$this->db->from($table);
	   	$this->db->where($where, $params);
		$this->db->where($where1, $params1);
	   	$res = $this->db->get();
	   	if($res->num_rows()>0)
	   	{
	    	$row = $res->row();
	     	return $row->$field;
	   	}
	   	else
	   	{
	     	return '';
	   	}	
	}

	function get_get($query)
	{
	 	$d= $this->db->query($query);
	 	return $d->row();
	}
	
	function get_one_fields($field, $table, $where, $params,$where2, $params2,$where3, $params3)
	{
	 	$this->db->select($field);
	   	$this->db->from($table);
	   	$this->db->where($where, $params);
		$this->db->where($where2, $params2);
		$this->db->where($where3, $params3);
	   	$res = $this->db->get();
	   	if($res->num_rows()>0)
	   	{
	    	$row = $res->row();
	     	return $row->$field;
	   	}
	   	else
	   	{
	     	return '';
	   	}	
	}
	
	
	
	function delete_one($key, $table, $param)
	{	
		$update = array(
    		'stat' => '0'
		);

		$this->db->where($key, $param);
		$this->db->update($table, $update);
	}

	function inisialisasi_delete($table, $where, $params)
	{
		$this->db->select('count(*) as total');
	   	$this->db->from($table);
	   	$this->db->where($where, $params);
	   	$res = $this->db->get();
	   	$row = $res->row();
	   	if($row->total == 0) {
	   		return true;
	   	} else {
	   		return false;
	   	}
	}

	function insert_validation($table, $where, $params)
	{	
		$this->db->select('count(*) as total');
	   	$this->db->from($table);
	   	$this->db->where(trim($where," "), trim($params, " "));
	   	$res = $this->db->get();
	   	$row = $res->row();
	   	if($row->total != 0) {
	   		return true;
	   	} else {
	   		return false;
	   	}
	}

	function counting_data($table, $where, $params)
	{
		$this->db->select('count(*) as total');
	   	$this->db->from($table);
	   	$this->db->where($where, $params);
	   	$res = $this->db->get();
	   	return $res->row()->total;
	}
 function pertanyaan(){
 	$query = $this->db->query("SELECT gejala as total from detail_gejala a JOIN tb_pengetahuan b on a.kode_indikator=b.pertanyaan");
 	return $query->row()->total;
 }

 function jikaya(){
   $query = $this->db->query("SELECT indikator as total from tb_indikator a JOIN tb_pengetahuan b on a.kode_indikator=b.jikaya");
   return $query->row()->total;
 }

 function jikatidak(){
   $query = $this->db->query("SELECT indikator as total from tb_indikator a JOIN tb_pengetahuan b on a.kode_indikator=b.jikatidak");
   return $query->row()->total;
 }
	
}