<?php 

class M_login extends CI_Model{	
	function cek_login($table,$where){		
		return $this->db->get_where($table,$where);
	}	

	function profil($user){
		$this->db->select('*');
		$this->db->from('user');
		
		$this->db->where('username',$user);
		$query = $this->db->get();
		return $query->row();
	}

}