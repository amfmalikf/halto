<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Halto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<!--Less styles -->
   <!-- Other Less css file //different less files has different color scheam
  <link rel="stylesheet/less" type="text/css" href="themes/less/simplex.less">
  <link rel="stylesheet/less" type="text/css" href="themes/less/classified.less">
  <link rel="stylesheet/less" type="text/css" href="themes/less/amelia.less">  MOVE DOWN TO activate
  -->
  <!--<link rel="stylesheet/less" type="text/css" href="themes/less/bootshop.less">
  <script src="themes/js/less.js" type="text/javascript"></script> -->
  
<!-- Bootstrap style --> 
    <link id="callCss" rel="stylesheet" href=" <?php echo base_url('assets/assets/front_end/themes/bootshop/bootstrap.min.css') ?>" media="screen"/>
    <link href="<?php echo base_url('assets/assets/front_end/themes/css/base.css') ?> " rel="stylesheet" media="screen"/>
<!-- Bootstrap style responsive --> 
  <link href="<?php echo base_url('assets/assets/front_end/themes/css/bootstrap-responsive.min.css') ?>" rel="stylesheet"/>
  <link href="<?php echo base_url('assets/assets/front_end/themes/css/font-awesome.css') ?>" rel="stylesheet" type="text/css">
<!-- Google-code-prettify --> 
  <link href="<?php echo base_url('assets/assets/front_end/themes/js/google-code-prettify/prettify.css') ?> " rel="stylesheet"/>
<!-- fav and touch icons -->
    <link rel="shortcut icon" href="<?php echo base_url('assets/assets/front_end/themes/images/ico/favicon.ico') ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/assets/front_end/themes/images/ico/apple-touch-icon-144-precomposed.png') ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/assets/front_end/themes/images/ico/apple-touch-icon-114-precomposed.png') ?> ">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/assets/front_end/themes/images/ico/apple-touch-icon-72-precomposed.png') ?> ">
    <link rel="apple-touch-icon-precomposed" href=" <?php echo base_url('assets/assets/front_end/themes/images/ico/apple-touch-icon-57-precomposed.png') ?>">
  <style type="text/css" id="enject"></style>

  </head>
<body >
<div id="header">
<div class="container">

<!-- Navbar ================================================== -->
<div id="logoArea" class="navbar">
<a id="smallScreen" data-target="#topMenu" data-toggle="collapse" class="btn btn-navbar">
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
</a>
  <div class="navbar-inner">
    <a class="brand" href="<?php echo base_url('home');?>"><h4><font color="#ffffff" >HALTO</font></h4></a>
    <ul id="topMenu" class="nav pull-right">
   <li class=""><a href="<?php echo base_url('#');?>">history</a></li>
   <li class=""><a href="<?php echo base_url('#');?>">pemesanan</a></li>
   <li class=""><a href="<?php echo base_url('#');?>">login</a></li>
   <li class="">
   
  </li>
    </ul>
  </div>
</div>
</div>
</div>
<!-- Header End====================================================================== -->
<div id="carouselBlk">
  <div id="myCarousel" class="carousel slide">
    <div class="carousel-inner">
      <div class="item active">
      <div class="container">
      <a href="#"><img style="width:100% height 40%" src="<?php echo base_url('assets/img/image1.jpg') ?>" alt="special offers"/></a>
      <div class="carousel-caption">
          <h4>Second Thumbnail label</h4>
          <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
        </div>
      </div>
      </div>
      
      <div class="item">
      <div class="container">
      <a href="#"><img style="width:100% height 40%" src="<?php echo base_url('assets/img/image2.jpg') ?>" alt="special offers"/></a>
      <div class="carousel-caption">
          <h4>Second Thumbnail label</h4>
          <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
        </div>
      </div>
      </div>

     <!--  <div class="item ">
      <div class="container">
      <a href="#"><img style="width:100% height 40%" src="<?php echo base_url('assets/img/image3.jpg') ?>" alt="special offers"/></a>
      <div class="carousel-caption">
          <h4>Second Thumbnail label</h4>
          <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
        </div>
      </div>
      </div> -->

    </div>
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div> 
</div>


<div id="mainBody">
   
      
 <?= $main ?>
   

   
</div>

      





<!-- Footer ================================================================== -->
  <div  id="footerSection">
  <div class="container">
    <div class="row">
      
      <div class="span3">
      <h5>INFORMATION</h5>
      <p>022-2788967<br>
         0821269134
         <!-- mutmuttourandtravel@gmail.com
         Jl Raya Lembang No.239 -->

         </p>
        
      </div>
    
      <div id="socialMedia" class="span3 pull-right">
        <h5>SOCIAL MEDIA </h5>
        <a href="#"><img width="60" height="60" src=" <?php echo base_url('assets/assets/front_end/themes/images/facebook.png') ?>" title="facebook" alt="facebook"/></a>
       <!--  <a href="#"><img width="60" height="60" src="<?php echo base_url('assets/assets/front_end/themes/images/twitter.png') ?>" title="twitter" alt="twitter"/></a>
        <a href="#"><img width="60" height="60" src="<?php echo base_url('assets/assets/front_end/themes/images/youtube.png') ?>" title="youtube" alt="youtube"/></a> -->
       </div> 
     </div>
    <p class="pull-right">&copy; Bootshop</p>
  </div><!-- Container End -->
  </div>
<!-- Placed at the end of the document so the pages load faster ============================================= -->
  <script src="<?php echo base_url('assets/assets/front_end/themes/js/jquery.js') ?>" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/assets/front_end/themes/js/bootstrap.min.js') ?>" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/assets/front_end/themes/js/google-code-prettify/prettify.js') ?>"></script>
  
  <script src="<?php echo base_url('assets/assets/front_end/themes/js/bootshop.js') ?>"></script>
    <script src="<?php echo base_url('assets/assets/front_end/themes/js/jquery.lightbox-0.5.js') ?>"></script>
    <script src="<?php echo base_url('assets/assets/isotope/jquery.isotope.js') ?>"></script>

  
  
<span id="themesBtn"></span>
</body>
</html>