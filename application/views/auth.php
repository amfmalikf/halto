<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BPT - Balai Perlindungan Tanaman</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?php echo base_url("assets/adminlte/css/css/bootstrap.min.css") ?>">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="<?php echo base_url("assets/adminlte/css/css/bootstrap.min.css") ?>">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?php echo base_url("assets/adminlte/css/css/style.default.css")?>" id="theme-stylesheet" ">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?php echo base_url("assets/adminlte/css/css/custom.css") ?> ">
    <!-- Favicon-->
    <link rel="shortcut icon" href="<?php echo base_url("assets/adminlte/img/favicon.ico") ?> ">

    <link href="<?php echo base_url('assets/adminlte/font-awesome-4.3.0/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />

    <!-- Font Awesome CDN-->
    <!-- you can replace it by local Font Awesome-->
    
    <!-- Font Icons CSS-->
    <link rel="stylesheet" href="<?php echo base_url("assets/adminlte/css/css/icons.css") ?> ">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                  <!--   <h1>One HEART</h1> -->
                  </div>
                  <!-- <p>Kebersamaan dengan Dukungan untuk Meraih Mimpi.</p> -->
                  <!-- <center><img style="margin-left: 100px;" src="<?php echo base_url("assets/adminlte/img/ahas.png") ?>" width="300" height="300"></center> -->
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                   <form action="<?php echo base_url('login/aksi_login'); ?>" method="post">
                    <div class="form-group">
                      <input id="login-username" type="text" name="username" required="" class="input-material">
                      <label for="login-username" class="label-material">UserName</label>
                    </div>
                    <div class="form-group">
                      <input id="login-password" type="password" name="password" required="" class="input-material">
                      <label for="login-password" class="label-material">Password</label>
                    </div><button class="btn btn-md btn-danger">login</button>
                    <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                  </form><a href="#" class="forgot-pass">Forgot Password?</a><br><small>Do not have an account? </small><a href="<?php echo site_url("error") ?>" class="signup">Signup</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyrights text-center">
        <p>Design by <a href="#" class="external">Bootstrapious</a></p>
        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
      </div>
    </div>
    <!-- Javascript files-->
    <script src="<?php echo base_url("assets/adminlte/js/jquery.min.js")?>"></script>
    <script src="<?php echo base_url("assets/adminlte/js/tether.min.js")?>"></script>
    <script src="<?php echo base_url("assets/adminlte/js/bootstrap.js")?>"></script>
    <script src="<?php echo base_url("assets/adminlte/js/jquery.cookie.js")?>"> </script>
    <script src="<?php echo base_url("assets/adminlte/js/jquery.validate.min.js")?>"></script>
    <script src="<?php echo base_url("assets/adminlte/js/front.js")?>"></script>
    <!-- <script src="<?php // echo base_url("assets/adminlte/js/fontAwesome.js") ?>"></script> -->
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
    <!---->
    <!-- <script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='//www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X');ga('send','pageview');
    </script> -->
  </body>
</html>