
        
<style type="text/css">
  
    {
    box-sizing: border-box;
}

body {
    background-color: #f1f1f1;
    padding: 20px;
    font-family: Arial;
}

/* Center website */
.main {
    max-width: 1000px;
    margin: auto;
}

h1 {
    font-size: 50px;
    word-break: break-all;
}

.row {
    margin: 8px -16px;
}

/* Add padding BETWEEN each column (if you want) */
.row,
.row > .column {
    padding: 8px;
}

/* Create three equal columns that floats next to each other */
.column {
    float: left;
    width: 18%;
    display: none; /* Hide columns by default */
}

/* Clear floats after rows */ 
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Content */
.content {
    background-color: white;
    padding: 10px;
}

/* The "show" class is added to the filtered elements */
.show {
    display: block;
}

/* Style the buttons */
.btn {
  border: none;
  outline: none;
  padding: 12px 16px;
  background-color: white;
  cursor: pointer;
}

/* Add a grey background color on mouse-over */
.btn:hover {
  background-color: #ddd;
}

/* Add a dark background color to the active button */
.btn.active {
  background-color: #666;
   color: white;
}
</style>
<div class="container">

    <div class="span12">  
      
<h4>Daftar Domba </h4>
<<div id="myBtnContainer">
  <button class="btn active" onclick="filterSelection('all')"> Show all</button>
 <?php foreach  ($allFile as $a) {?>

  <button class="btn" onclick="filterSelection('<?= $a['kelas'];?>')"><?= $a['kelas'];?></button>
  <?php }?>
</div>

<!-- Portfolio Gallery Grid -->
<div class="row">
    <?php foreach  ($allFile as $a) {?>

    <div class="column <?= $a['kelas'];?>">
      <div class="content">
         <img data-tooltip="tooltip" title="Lihat Gambar" width="80%" src="<?php echo base_url('../halto/assets/img/domba/'.$a['gambar']);?>" alt="">

        <h4><?= $a['kelas'];?></h4>
        <p><?= $a['harga_dasar'];?></p>
        <a onclick="pilih('<?php echo $a['id_reg'];?>')" class="btn btn-sm- btn-info "  >info</a>
      </div>
    </div>
  <?php }?>

</div>
     <!-- END GRID -->
<hr>
     <h4>Daftar Sapi </h4>
<div id="myBtnContainer">
  <button class="btn active" onclick="filterSelection('all')"> Show all</button>
 <?php foreach  ($allFile2 as $b) {?>

  <button class="btn" onclick="filterSelection('<?= $b['kelas'];?>')"><?= $b['kelas'];?></button>
  <?php }?>
</div>

<!-- Portfolio Gallery Grid -->
<div class="row">
    <?php foreach  ($allFile2 as $b) {?>

    <div class="column <?= $b['kelas'];?>">
      <div class="content">
         <img data-tooltip="tooltip" title="Lihat Gambar" width="80%" src="<?php echo base_url('../halto/assets/img/sapi/'.$b['gambar']);?>" alt="">
        <h4><?= $b['kelas'];?></h4>
        <p><?= $b['harga_dasar'];?></p>
        <a onclick="pilih('<?php echo $b['id_reg'];?>')" class="btn btn-sm btn-info "  >info</a>
      </div>
    </div>
  <?php }?>


</div>
     <!-- END GRID -->


    </div>
</div>

<script>
filterSelection("all")
function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("column");
  if (c == "all") c = "";
  for (i = 0; i < x.length; i++) {
    w3RemoveClass(x[i], "show");
    if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
  }
}

function w3AddClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {element.className += " " + arr2[i];}
  }
}

function w3RemoveClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);     
    }
  }
  element.className = arr1.join(" ");
}


// Add active class to the current button (highlight it)
var btnContainer = document.getElementById("myBtnContainer");
var btns = btnContainer.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function(){
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}
</script>


      <div class="modal modal-large fade" id="upload_modal" data-backdrop="">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 id="tittle" class="modal-title"></h3>
                    </div>
                    <div class="modal-body uploadme">
                            
                    </div>
                    <div class="modal-footer" id="modal_footer">  
                        
                    </div>
                </div> 
            </div>
        </div>

<script type="text/javascript">
function pilih(id) {
  

     $.ajax({
                        url : "<?=base_url()?>maps/pending_form/" +id ,
                        //type: "GET",
                        data:null,
                        dataType: "html",
                        success: function(data)
                        {   
                           
                            document.getElementById('tittle').innerHTML = 'Detail ';
              
              $('.uploadme').html(data);
                           
                            
                            $('#upload_modal').modal('show');
                        },
                        error: function (data)
                        {
                            alert('Error get data from ajax');
                        }
            });

    
 }
 </script>