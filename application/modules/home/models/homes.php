<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of lokasi
 * @created on : Thursday, 26-Jul-2018 01:02:35
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2018    
 */
 
 
class homes extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }

    public function get_all() 
    {
        
        $result = $this->db->query(" SELECT * FROM tbl_noreg_domba ");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        { 
            return array();
        } 
    } 

    public function get_sapi() 
    {
        
        $result = $this->db->query(" SELECT * FROM tbl_noreg_sapi ");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        { 
            return array();
        } 
    } 

}
