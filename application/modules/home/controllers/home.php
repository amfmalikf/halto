<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller lokasi
 * @created on : Thursday, 26-Jul-2018 01:02:35
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2018
 *
 *
 */


class home extends MY_Controller
{

    public function __construct()  
    {
        parent::__construct();         
        $this->load->model('homes');
    }
    

    /**
    * List all data lokasi
    *
    */
    public function index() 
    {
        $data['allFile'] = $this->homes->get_all();
        $data['allFile2'] = $this->homes->get_sapi();
        $this->template->render('home/view',$data);
	      
    }

    

}

?>
