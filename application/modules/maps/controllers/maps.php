<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller lokasi
 * @created on : Thursday, 26-Jul-2018 01:02:35
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2018
 *
 *
 */


class maps extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();         
        $this->load->model('mapss');
    }
    

    /**
    * List all data lokasi
    *
    */

    public function index()
    {   
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['as'] = $this->mapss->get_all2();
        // $data['l'] = $this->db->query("SELECT * FROM lokasi GROUP BY nama");
        $this->render_page('maps',$data);

    }

    public function pending_form($id)
 
    {

        $data['no_retur']=$id;    
        // $data['a']=$id;    
         
        $this->load->view('maps/action_pending',$data);
    }

}

?>
