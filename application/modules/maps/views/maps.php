<div role="main" class="main">  
        <div class="container">
          <div class="card mb-2">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-8">
                    
                      <!-- <b>Opsi Peta: </b>
                      <input type="checkbox" id="layer_01" checked="checked" /> Batas Kecamatan
                      <input type="checkbox" id="layer_02" checked="checked" /> Batas Kampun -->
                      <div id="map_canvas" style="width: auto; height: 600px;"></div>
                      <!-- <div id="status"></div> -->
                   <!--    <div align="right"> <br>    
                      <button id="find_btn" class="btn btn-primary btn-flat"> <i class="fa fa-map-marker"></i> Lokasi Saya</button>          
                      </div> -->
              

                  </div>
                  <div class="col-lg-4">
                    <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      
                        <i class="fa fa-database"></i>
                        Seputar Top 5  Organisme Pengganggu Tanaman
                 
                    </h4>
                  </div>
                  <div id="collapseOne" class="accordion-body collapse in">
                    <div class="panel-body">
                      <?php if ($as) : ?>
                                <table id="example1" class="table table-hover table-condensed">
                                  <thead>
                                    <th>No</th>
                                    <th>Nama OPT</th>
                                    <th>Komoditi</th>
                                    <th>Luas Serangan</th>
                                    <th>Keterangan</th>
                                  </thead>
                                   <tbody>
                                   
                                    <?php foreach ($as as $s) : ?>
                                      <tr>
                                       <td><?php echo $number++;; ?> </td>
                                       
                                       <td><?php echo $s['nama']; ?></td>
                                       
                                       <td><?php echo $s['nama_kategori']; ?></td>
                                       <td><?php echo $s['telp']; ?></td>
                                       <td> <a onclick="pilih('<?php echo $s['id'];?>')" class="btn btn-sm btn-info "  >info</a></td>
                                      </tr>     
                                    <?php endforeach; ?>
                                  </tbody>
                                </table>
                                <?php else: ?>
                                      <?php  echo notify('Data lokasi belum tersedia','info');?>
                                <?php endif; ?>
                    </div>
                  </div>
                </div>
                    
                  </div>

                </div>   
              </div>
             
            </div>
        </div>
        
      </div>

      <div class="modal modal-large fade" id="upload_modal" data-backdrop="">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 id="tittle" class="modal-title"></h3>
                    </div>
                    <div class="modal-body uploadme">
                            
                    </div>
                    <div class="modal-footer" id="modal_footer">  
                        
                    </div>
                </div> 
            </div>
        </div>

<script type="text/javascript">
function pilih(id) {
  

     $.ajax({
                        url : "<?=base_url()?>maps/pending_form/" +id ,
                        //type: "GET",
                        data:null,
                        dataType: "html",
                        success: function(data)
                        {   
                           
                            document.getElementById('tittle').innerHTML = 'Detail ';
              
              $('.uploadme').html(data);
                           
                            
                            $('#upload_modal').modal('show');
                        },
                        error: function (data)
                        {
                            alert('Error get data from ajax');
                        }
            });

    
 }
 </script>