<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of lokasi
 * @created on : Thursday, 26-Jul-2018 01:02:35
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2018    
 */
 
 
class mapss extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }


     public function get_all2() 
    {
        
        $result = $this->db->query(" SELECT * FROM lokasi a JOIN kategori b on a.kategori=b.id_kategori");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        { 
            return array();
        } 
    } 
}
