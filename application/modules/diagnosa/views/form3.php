<div role="main" class="main">
	<div class="container">
        <div class="card mb-1">
			<div class="card-body">
                <div class="row">
                  <div class="col-lg-12">

                     <h2>Hasil diagnosa menunjukan bahwa perkebunan anda terserang OPT <b><u><?= $result['nama_HamaPenyakit']; ?></u></b> </h2>


                  </div>
                  <div class="col-lg-4">

                  </div>
                </div>

                 
						<div class="panel-group" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle " data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
										<i class="fa fa-bug"></i>
										Deskripsi OPT
									</a>
								</h4>
							</div>

							<div id="collapseOne" class="accordion-body collapse in">
								<div class="panel-body">
									<table class="table">
										
										<tbody>

											<tr>

											<td style="text-align:justify; padding-left:30px;font-size:18px;"><?= $result['deskripsi']; ?></td>

												 <td style="font-size:18px">
												 	<div class="tz-gallery">
								                           <a class="lightbox " href="<?php echo base_url('assets/fileGambarVideo/'.$result['gambar_video']);?>">
								                               <img style="width: 170px; height: 200px;" data-tooltip="tooltip" title="Lihat Gambar" src="<?php echo base_url('assets/fileGambarVideo/'.$result['gambar_video']);?>"  alt="Park">
								                           </a>
								                     </div>
												 </td>
												 
											</tr>
									
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTri">
										<i class="fa fa-bug"></i>
										Ciri-Ciri Serangan OPT
									</a>
								</h4>
							</div>

							<div id="collapseTri" class="accordion-body collapse  ">
								<div class="panel-body">
									<table class="table">
										<thead>
											<th style="font-size:20px">Gejala</th>
											<th style="font-size:20px">Gambar</th>
										</thead>
										<tbody>

											<?php foreach ($result_detail as $key): ?>
											<tr>
												 <td style="font-size:18px"><?= $key['gejala'] ?></td>
												 <td>
													 <div class="tz-gallery">
	 	                           <a class="lightbox " href="<?php echo base_url('assets/img/file_gelaja/'.$key['img_gejala']);?>">
	 	                               <img style="width: 170px; height: 200px;" data-tooltip="tooltip" title="Lihat Gambar" src="<?php echo base_url('assets/img/file_gelaja/'.$key['img_gejala']);?>"  alt="Park">
	 	                           </a>
	 	                     </div>
												 </td>
											</tr>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										<i class="fa fa-leaf"></i>
										Cara Pengendalian
									</a>
								</h4>
							</div>

							<div id="collapseTwo" class="accordion-body collapse  ">
								<div class="panel-body">




											<table class="table">
												<thead>

													<th width="15%" style="font-size:20px">Teknik</th>
													<th width="15%" style="font-size:20px">Metode</th>
													<th width="25%" style="font-size:20px">	Deskripsi</th>
													<th width="10%" style="font-size:20px">	keterangan</th>
													<th width="15%" style="font-size:20px">	Foto</th>
												</thead>

												<tbody>
<?php foreach ($result_detail3 as $key): ?>
	<?php if ($key['teknik']=='1') {
                 $st = '<p style="font-size:18px">Mekanis</p>';
               }
               elseif ($key['teknik']=='2'){
                $st = '<p style="font-size:18px">Biologis</p>';
                } else{
                $st = '<p style="font-size:18px">Fisik</p>';}?>
													<tr>

														<td style="font-size:18px"><?php echo $st;?></td>
														<td style="font-size:18px"><?= $key['nama'] ?></td>
														<td style="font-size:18px;" style="text-align:justify"><p style="text-align:justify"><?= $key['deskripsi'] ?></p></td>
														 <td style="font-size:18px"><?= $key['keterangan'] ?></td>
														 <td>
															 <div class="tz-gallery">
						                     <div class="thumbnail">
																	 <a class="lightbox " href="
																	 <?php if($key['gambar']==''){echo base_url('assets/img/file_pengendalian/nophoto.png');}else{ echo base_url('assets/img/file_pengendalian/'.$key['gambar']);}?>">

																	
																		<img style="width: 170px; height: 200px;" data-tooltip="tooltip" title="Lihat Gambar" src="<?php if($key['gambar']==''){echo base_url('assets/img/file_pengendalian/nophoto.png');}else{ echo base_url('assets/img/file_pengendalian/'.$key['gambar']);}?>"  alt="Park">
																	 </a>
														 </div>
													 </div>
														 </td>
													</tr>
												<?php endforeach; ?>

												</tbody>
											</table>


									</div>



								</div>
							</div>
						</div>
					</div>

        </div></div>
</div>
