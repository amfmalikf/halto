<div role="main" class="main">
	<div class="container">
        <div class="card mb-1">
			<div class="card-body">
                <div class="row">
                  <div class="col-lg-9">

                    
					<label style="font-size:17pt; color: black;">
						<?php echo  $question['gejala'];?>
					</label>
					<a href="<?= base_url("diagnosa/show_next/". $question['jikaya']) ?>" class="btn btn-danger btn-md">YA</a>
					<a href="<?= base_url("diagnosa/show_next/". $question['jikatidak']) ?>" class="btn btn-primary btn-md">TIDAK</a>


                  </div>
                  <div class="col-lg-3">
										
						<div class="tz-gallery">
							
								<a class="lightbox" href="<?php echo base_url('assets/img/file_gelaja/'.$question['img_gejala']);?>">
										<img style="width: 170px; height: 200px;" data-tooltip="tooltip" title="Lihat Gambar" src="<?php echo base_url('assets/img/file_gelaja/'.$question['img_gejala']);?>"  alt="Park">
								</a>
					
						</div>
				  </div>
                </div>
              </div>

            </div>
        </div>

</div>
