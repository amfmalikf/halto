<div role="main" class="main">  
        <div class="container">
          <div class="card mb-2">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-8">
                    
                      <!-- <b>Opsi Peta: </b>
                      <input type="checkbox" id="layer_01" checked="checked" /> Batas Kecamatan
                      <input type="checkbox" id="layer_02" checked="checked" /> Batas Kampun -->
                      <div id="map_canvas" style="width: auto; height: 600px;"></div>
                      <div id="status"></div>
                   <!--    <div align="right"> <br>    
                      <button id="find_btn" class="btn btn-primary btn-flat"> <i class="fa fa-map-marker"></i> Lokasi Saya</button>          
                      </div> -->
              

                  </div>
                  <div class="col-lg-4">
                    <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      
                        <i class="fa fa-database"></i>
                        Daftar Organisme Pengganggu Tanaman
                
                    </h4>
                  </div>
                  <div id="collapseOne" class="accordion-body collapse in">
                    <div class="panel-body">
                      <?php if ($as) : ?>
                                <table class="table table-hover table-condensed">
                                  <thead>
                                    <th>No</th>
                                    <th>Nama OPT</th>
                                    <th>Komoditi</th>
                                    <th>Luas Serangan</th>
                                  </thead>
                                   <tbody>
                                   
                                    <?php foreach ($as as $s) : ?>
                                      <tr>
                                       <td><?php echo $number++;; ?> </td>
                                       
                                       <td><?php echo $s['nama']; ?></td>
                                       
                                       <td><?php echo $s['nama_kategori']; ?></td>
                                       <td><?php echo $s['telp']; ?></td>
                                      </tr>     
                                    <?php endforeach; ?>
                                  </tbody>
                                </table>
                                <?php else: ?>
                                      <?php  echo notify('Data lokasi belum tersedia','info');?>
                                <?php endif; ?>
                    </div>
                  </div>
                </div>
                    
                  </div>

                </div>   
              </div>
             
            </div>
        </div>
        
      </div>