<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tb_indikator
 * @created on : Thursday, 26-Jul-2018 12:36:52
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2018
 */


class diagnosas extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }


    public function get_question_first()
    {
        $this->db->select("*");
        $this->db->from("detail_gejala");
        $this->db->where('id_gejala', '1');
        $result = $this->db->get();

        if ($result->num_rows() == 1)
        {
            return $result->row_array();
        }
        else
        {
            return array();
        }
    }

    public function get_question_next($id)
    {
        $this->db->select("*");
        $this->db->from("detail_gejala");
        $this->db->where('id_gejala', $id);
        $result = $this->db->get();

        if ($result->num_rows() == 1)
        {
            return $result->row_array();
        }
        else
        {
            return array();
        }
    }
    public function get_result($id)
    {
        $this->db->select("*");
        $this->db->from("hama_penyakit a");
        $this->db->join("filegambar_video b","a.id_HamaPenyakit=b.id_HamaPenyakit");
        // $this->db->join("detail_gejala c","a.id_HamaPenyakit=c.id_HamaPenyakit");
        $this->db->where('a.id_HamaPenyakit', $id);
        $result = $this->db->get();

        if ($result->num_rows() == 1)
        {
            return $result->row_array();
        }
        else
        {
            return array();
        }
    }
    public function get_detail($id)
    {
        $this->db->select("*");
        $this->db->from("detail_gejala a");
        $this->db->join("hama_penyakit b","a.id_HamaPenyakit=b.id_HamaPenyakit");
        // $this->db->join("detail_gejala c","a.id_HamaPenyakit=c.id_HamaPenyakit");
        $this->db->where('a.id_HamaPenyakit', $id);
        $result = $this->db->get();

        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return array();
        }
    }
    public function get_detail2($id)
    {
        $this->db->select("*");
        $this->db->from("pengendalian a");
        $this->db->join("hama_penyakit b","a.id_HamaPenyakit=b.id_HamaPenyakit");
        // $this->db->join("detail_gejala c","a.id_HamaPenyakit=c.id_HamaPenyakit");
        $this->db->where('a.id_HamaPenyakit', $id);
        $this->db->group_by('a.teknik', $id);
        $result = $this->db->get();

        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return array();
        }
    }
    public function get_detail3($id)
    {
        $this->db->select("*");
        $this->db->from("pengendalian a");
        // $this->db->join("hama_penyakit b","a.id_HamaPenyakit=b.id_HamaPenyakit");
        // $this->db->join("detail_gejala c","a.id_HamaPenyakit=c.id_HamaPenyakit");
        $this->db->where('a.id_HamaPenyakit', $id);
        // $this->db->where('a.teknik', $id);
        // $this->db->group_by('a.id_HamaPenyakit', $id);
        $result = $this->db->get();
   
        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return array();
        }
    }


    // SELECT a.nama_HamaPenyakit from hama_penyakit a join detail_gejala b on a.id_HamaPenyakit=b.id_HamaPenyakit WHERE b.id_gejala=1

     public function get_all2()
    {

        $result = $this->db->query(" SELECT * FROM lokasi a JOIN kategori b on a.kategori=b.id_kategori WHERE a.id");

        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return array();
        }
    }















}
