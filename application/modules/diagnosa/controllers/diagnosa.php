<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller lokasi
 * @created on : Thursday, 26-Jul-2018 01:02:35
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2018
 *
 *
 */


class diagnosa extends MY_Controller 
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('diagnosas');
    }

 
    /**
    * List all data lokasi
    *
    */

    public function index()
    {
      $data['question'] = $this->diagnosas->get_question_first();
      $data['as'] = $this->diagnosas->get_all2();
      // $data['number']         = (int)$this->uri->segment(3) +1;


      $this->template->render("form",$data);
    }
    
    public function show_next($id)
    {
      $cek = $this->global_m->get_one_field('gejala','detail_gejala','id_gejala',$id);
      if ($cek=="") {
        $data['result'] = $this->diagnosas->get_result($id);
        $data['result_detail'] = $this->diagnosas->get_detail($id);
        $data['result_detail2'] = $this->diagnosas->get_detail2($id);
        $data['result_detail3'] = $this->diagnosas->get_detail3($id); 
        $this->template->render('form3',$data);
      }else {
        $data['question_next'] = $this->diagnosas->get_question_next($id);
        $this->template->render('form2',$data);
      }

    }

}

?>
