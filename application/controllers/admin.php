<?php 

class Admin extends CI_Controller{

	function __construct(){
		parent::__construct();
	
		if($this->session->userdata('stat') != "login"){
			redirect(base_url("login"));
		}
	}

	function index(){
		redirect(site_url('dashboard'));
	}
}