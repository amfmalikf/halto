
<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller Dashboard
 * @created on : 16-05-2014
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2014
 *
 *
 */


class index_viewers extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct();  
        $this->load->model('index_viewerss');        
       
    }  
    

    /**
    * List all data pegawai
    *
    */
    public function index()
    {

        
        $data['allFile'] = $this->index_viewerss->get_all();
        $this->load->view('home',$data);

 
    }

     public function diagnosa()
    {

        
        // $data['allFile'] = $this->index_viewerss->get_all();
        $this->load->view('diagnosa');

 
    }

    public function hasdiag() 
    {

        
        // $data['allFile'] = $this->index_viewerss->get_all();
        $this->load->view('hasdiag');


    }

    public function katalog($id)
    {
        $data['getall'] = $this->index_viewerss->get_one1($id);
        $data['allFile'] = $this->index_viewerss->get_all();
        $data['gej'] = $this->index_viewerss->get_dk($id);
        $data['pen'] = $this->index_viewerss->get_dkk($id);

        $this->load->view('katalog',$data);
    }

    public function maps2()
    {   
        $data['number']         = (int)$this->uri->segment(3) +1;
        $data['as'] = $this->index_viewerss->get_all2();
        $data['l'] = $this->db->query("SELECT * FROM lokasi GROUP BY nama");
        $this->load->view('maps',$data);

    }


    
    
}

?>
