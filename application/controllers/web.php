<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class web extends CI_Controller {

	public function index()
	{
		$data['l'] = $this->db->query("SELECT * FROM lokasi_serangan GROUP BY nama_opt");
		$this->template->load('maps/_template','maps/_home',$data);
	}

	public function profil()
	{
		$data['p'] = $this->db->get_where("profil",array("id_profil"=>1))->row_array();
		$this->template->load('front-end/_template','front-end/_profil',$data);
	}

	public function lokasi()
	{
		$data['la'] = $this->db->query("SELECT l.id, l.nama, l.gambar, l.alamat, l.telp, l.latittude, l.longitude, l.tahun, k.nama_kategori, k.ikon
            							FROM lokasi as l, kategori as k
            							WHERE l.kategori=k.id");
		$this->template->load('front-end/_template','front-end/_lokasi',$data);
	}

	public function lokasi_one($id)
	{
		$data['lo'] = $this->db->get_where("lokasi",array("id"=>$id))->row_array();
		$this->template->load('front-end/_template','front-end/_lokasi_one',$data);
	}

	public function komentar()
	{
		if(isset($_POST['kirim'])) {
			$data = array(
				'nama' 		=> $this->input->post('nama'),
				'email' 	=> $this->input->post('email'),
				'telp' 		=> $this->input->post('telp'),
				'komentar' 	=> $this->input->post('komentar'),
				);
			$this->db->insert('komentar',$data);
			redirect('web/komentar');
        }else{
        	$data['k'] = $this->db->query("SELECT * FROM komentar GROUP BY id_komentar DESC LIMIT 5");
			$this->template->load('front-end/_template','front-end/_komentar',$data);
        }
	}

	public function direction()
	{
		if(isset($_POST['cari'])) {
			$data = array(
				'awal' => $_POST['asal'],
				'ahir' => $_POST['tujuan'],
				'l'		=> $this->db->query("SELECT * FROM lokasi GROUP BY nama"),
				'la' 	=> $this->db->query("SELECT l.id, l.nama, l.gambar, l.alamat, l.telp, l.latittude, l.longitude, l.tahun, k.nama_kategori, k.ikon
				FROM lokasi as l, kategori as k
				WHERE l.kategori=k.id"),
				);
            $this->template->load('front-end/_template','front-end/_direction_v',$data);
        }else{
			$data['l'] = $this->db->query("SELECT * FROM lokasi GROUP BY nama");
			$data['la'] = $this->db->query("SELECT l.id, l.nama, l.gambar, l.alamat, l.telp, l.latittude, l.longitude, l.tahun, k.nama_kategori, k.ikon
			FROM lokasi as l, kategori as k
			WHERE l.kategori=k.id");
			$this->template->load('front-end/_template','front-end/_direction_v',$data);
        }
	}	

	
} 






































/*Copyright©2017 fajar.fffff@gmail.com*/